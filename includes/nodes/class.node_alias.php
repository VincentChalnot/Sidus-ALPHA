<?php

/**
 * Description of class
 *
 * @author vincent
 */
class node_alias extends node_generic {

	public function __construct(sys_config $config, $id) {
		$id = (int) $id;
		$this->config = $config;
		$this->public[] = 'pointed_node_id';

		parent::__construct($config, $id);
	}

	protected static function add_more($id, sys_config $config, node_generic $parent_node, $form) {
		$tmp = parent::add_more($id, $config, $parent_node, $form);
		if ($tmp == false) {
			return false;
		}
		$query = 'INSERT INTO node_alias (node_id,pointed_node_id) VALUES ('.(int) $id.','.(int) $form->get_value('pointed_node_id').')';
		if (!$config->db()->exec($query)) {
			$config->error()->add(30);
			return false;
		}
		return true;
	}

	protected function delete_more() {
		$tmp = parent::delete_more();
		if ($tmp == false) {
			return false;
		}
		$query = 'DELETE FROM node_alias WHERE node_id='.$this->infos['node_id'];
		if (!$this->config->db()->exec($query)) {
			return false;
		}
		return true;
	}

	protected function edit_more($form) {
		$tmp = parent::edit_more($form);
		if ($tmp == false) {
			return false;
		}
		$list[] = 'pointed_node_id=\''.$this->config->secure_input($this->form()->get_value('pointed_node_id')).'\'';
		$query = 'UPDATE node_alias SET '.implode(', ', $list).' WHERE node_id='.$this->infos['node_id'];
		if (!$this->config->db()->exec($query)) {
			return false;
		}
		return true;
	}

	protected function get_more() {
		parent::get_more();
		//Adding all node_alias properties
		$query = 'SELECT * FROM node_alias WHERE node_id='.$this->infos['node_id'];
		$this->infos = array_merge($this->infos, $this->config->db()->get_row($query));
	}

	protected function init_more_form() {
		parent::init_more_form();
		$nodes = array();
		require_once REAL_PATH.'includes/interface/class.ui_select.php';
		$query = 'SELECT node_id FROM node_generic WHERE 1 LIMIT 1000';
		$tmp = $this->config->db()->get_array($query);
		if ($tmp != false) {
			foreach ($tmp as $el) {
				$node = $this->config->node($el['node_id']);
				$nodes[$node->get('node_id')] = $node->get('node_id').' - '.$node.' ['.$node->get('type_name', false).']';
			}
		}
		$this->content_form->add(new ui_select('pointed_node_id', 'Pointed node :', $nodes, $this->get('pointed_node_id')), 1); //Don't forget to add more file type when available
	}

	public function get_pointed_node() {
		if ($this->config->node_exists($this->get('pointed_node_id'))) {
			return $this->config->node($this->get('pointed_node_id'));
		}
		return $this;
	}

	public function get_html_content() {
		if (!$this->auths['read']) {//Test permissions
			$this->add_read_error();
			return false;
		}
		$string = '<h3>This element is an alias of <a href="'.$this->get_pointed_node()->link().'">'.$this->get_pointed_node().'</a></h3>';
		$string.=$this->get('content');
		return $string;
	}

	public static function get_add_form($action = '', $method = 'post'){
		$form = parent::get_add_form($action, $method);
		require_once REAL_PATH.'includes/interface/class.ui_select.php';
		$nodes = array();
		$query = 'SELECT node_id FROM node_generic WHERE 1 LIMIT 1000';
		$tmp = sys_config::get_instance()->db()->get_array($query);
		if ($tmp != false) {
			foreach ($tmp as $el) {
				$node = sys_config::get_instance()->node($el['node_id']);
				$nodes[$node->get('node_id')] = $node->get('node_id').' - '.$node.' ['.$node->get('type_name', false).']';
			}
		}
		$form->add(new ui_select('pointed_node_id', 'Pointed node :', $nodes), 2); //Don't forget to add more file type when available
		return $form;
	}

}

?>

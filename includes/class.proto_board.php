<?php

date_default_timezone_set('Europe/Paris'); //This is just to prevent error messages from picky apache servers

/**
 * This is the abstraction class for your main class.
 * TODO: rewrite this definition
 * Board is the main class you use for your projects,
 * it's the first level of abstraction behind the gr-
 * -aphical interface, you just have to instanciate
 * it in each of your applications.
 *
 * @author Vincent Chalnot
 * @version 1.0.1
 */

 /**
  * //You need to declare in a global constant the location of the the framework directory
  * define('REAL_PATH', dirname(dirname($_SERVER['SCRIPT_FILENAME'])).'/framework/');
  * define('HTTP_PATH', '/mywebsite/framework/');
  */

class proto_board{

	const CHARSET = 'UTF-8';

	protected $config;
	protected $controller; //class.proto_controller.php

	/**
	 * Simple constructor for the board class.
	 */
	public function __construct(){
		$this->init_board();
		$this->load_controller();
	}

	/**
	 * This function needs the right paths to the framework to be set with
	 * REAL_PATH and HTTP_PATH
	 */
	protected final function init_board(){
		require_once REAL_PATH.'includes/class.sys_config.php';
		$this->config=new sys_config($this);
		if($_SERVER['REMOTE_ADDR'] == $_SERVER['SERVER_ADDR']){
			$this->config->debug = true;
		}
	}

	/**
	 * Each application needs a different controller, as it will choose to
	 * redirect the user to the right page/view considering the action of
	 * the client. Still, you can you the prototype as a generic controller.
	 */
	protected function load_controller(){
		require_once REAL_PATH.'includes/class.proto_controller.php';
		$this->controller=new proto_controller($this->config);
	}

	/**
	 * This function is the core of your application, each time you will need
	 * to access a node, you will use this function.
	 * You need to define how the system choose the current node, here it uses
	 * the controller.
	 * @param (int) node_id
	 * @return (node_generic) <Object>
	 */
	public function node($id=null){
		return $this->config->node($id);
	}

	/**
	 * This function check if the node_id is correct
	 * @param (int) node_id
	 * @return (bool) exists
	 */
	public final function node_exists($id){
		return $this->config->node_exists($id);
	}

	/**
	 * Get the config handler
	 * @return (sys_config) <Object>
	 */
	public final function config(){
		return $this->config;
	}

	/**
	 * Get the user handler
	 * @return (node_user) <Object>
	 */
	public final function user(){
		return $this->config->user();
	}

	/**
	 * Get the error handler
	 * @return (sys_error) <Object>
	 */
	public final function error(){
		return $this->config->error();
	}

	/**
	 * Get the date handler
	 * @return (sys_date) <Object>
	 */
	public final function date(){
		return $this->config->date();
	}

	/**
	 * Get the session handler
	 * @return (sys_session) <Object>
	 */
	public final function session(){
		return $this->config->session();
	}

	/**
	 * Get the controller
	 * @return (proto_controller) <Object>
	 */
	public function controller(){
		return $this->controller;
	}

	/**
	 * Get the controller
	 * @return (proto_controller) <Object>
	 */
	public function form(){
		return $this->controller->get_form();
	}

	/**
	 * Get a global configuration variable
	 * @param (String) key
	 * @return (String) value
	 */
	public final function get_global($key){
		return $this->config->get($key);
	}

	public final function localize($string){
		return $this->config->localize($string);
	}

	/**
	 * Secure input to prevent XSS attacks
	 * This is just converting sensitive characters to their HTML equivalents
	 * @param (String) original
	 * @return (String) secured
	 */
	public function secure_display($string){
		return htmlspecialchars($string, ENT_COMPAT, 'UTF-8');
	}

	/**
	 * This function depends entirely on what kind of results you want to display
	 * You need to define for each field a specific search function: search_{field name}
	 */
	public function search($text, $table='title'){
		if(strlen($text) < 3){
			return false;
		}
		$method='search_'.$table;
		if(!method_exists($this, $method)){
			//TODO: Throw error
			return false;
		}
		return $this->$method($text);
	}

	public function search_title($text){
		$query='SELECT node_id FROM node_generic WHERE title LIKE \'%'.$this->config->secure_input($text).'%\'';
		return $this->parse_search($query);
	}

	public function search_tags($text){
		$query='SELECT node_id FROM node_info WHERE tags LIKE \'%'.$this->config->secure_input($text).'%\'';
		return $this->parse_search($query);
	}

	public function search_content($text){
		$query='SELECT node_id FROM node_info WHERE content LIKE \'%'.$this->config->secure_input($text).'%\'';
		return $this->parse_search($query);
	}

	public function search_username($text){
		$query='SELECT user_id AS node_id FROM node_user WHERE username LIKE \'%'.$this->config->secure_input($text).'%\'';
		return $this->parse_search($query);
	}

	/**
	 * Add your custom search functions at the bottom of this list
	 */

	/**
	 * This function return the actual nodes and prevent the search
	 * function from returning forbidden nodes in the results.
	 * @param (String) search_query
	 * @return (Array(node_generic)) nodes
	 */
	private final function parse_search($query){
		$nodes=array();
		foreach($this->config->db()->get_array($query.' LIMIT 100') as $value){
			$tmp=$this->config->node($value['node_id']);
			if($tmp->get_auth('read')){
				$nodes[]=$tmp;
			}
		}
		return $nodes;
	}

	/**
	 * Getting the current node from the controller
	 * @return (node_generic) current_node
	 */
	public function current_node(){
		return $this->controller()->current_node();
	}

	/**
	 * Getting the view path from the controller
	 * @return (String) view_path
	 */
	public function get_view_path(){
		return $this->controller()->get_view_path();
	}

	/**
	 * Getting the current form from the controller
	 */
	public function get_form(){
		return $this->controller()->get_form();
	}

	/**
	 * This function tries to gracefully shorten titles and short strings to
	 * the specified number of characters.
	 */
	public function shorten($string, $len=40){
		$tmp=explode(',', $string);
		$string=trim($tmp[0]);
		if(strlen($string) > $len){
			$tmp=substr($string, 0, $len + 1);
			$cut=strrpos($tmp, ' ');
			$tmp=substr($tmp, 0, $cut);
			if($tmp == ''){
				$tmp=substr($string, 0, $len);
			}
			return trim($tmp).'&hellip;';
		}
		return $string;
	}

	/**
	 * This function tries to gracefully shorten a long text to the specified
	 * number of characters.
	 */
	public function summarize($string, $len=300){
		if(strlen($string) > $len){
			$tmp=substr($string, 0, $len + 1);
			$cut=strrpos($tmp, '.',-1);
			$cut2=strrpos($tmp, ',',-1);
			if($cut2>$cut){
				$cut=$cut2;
			}
			$tmp=substr($tmp, 0, $cut);
			if($tmp == ''){
				$tmp=substr($string, 0, $len);
			}
			return trim($tmp).'&hellip;';
		}
		return $string;
	}

	/**
	 * You should use a common function to generate URLs to the nodes
	 */
	public function link($node, $options=array()){
		if(!$options && $node->get('type_name', false) == 'link'){
			return $node->get('media_url');
		}
		$link=PROJECT_HTTP_PATH.'index.php?node_id='.$node->get('node_id');
		if(!is_array($options)){
			$options=(string)$options;
			if($options != ''){
				if(strpos($options, '=')){
					trigger_error('Warning ! Passing "=" in link method instead of an array', E_USER_DEPRECATED);
				}
				$link.='&amp;'.urlencode($options);
			}
			return $link;
		}
		foreach($options as $key => $value){
			$link.='&amp;';
			if($key){
				$link.=urlencode($key).'=';
			}
			$link.=urlencode($value);
		}
		return $link;
	}

	/**
	 * Slugify a string, transform all special chars to the delimiter and lower-
	 * case the result. Try to replace all accentuated chars to their ASCII
	 * equivalent if Iconv exists and if the locale is set correctly
	 * @param string $string
	 * @param string $delimiter
	 * @return string
	 */
	public function slugify($string, $delimiter = '-') {
		$string = $this->replace_non_ascii((string)$string, $delimiter); // replace non letter or digits by the delimiter
		$string = strtolower($this->ascii_format(trim($string, $delimiter))); //trim, transliterate to ascii and to lowercase
		$string = preg_replace('~[^' . preg_quote($delimiter) . '\w]+~', '', $string); // remove unwanted characters
		if (empty($string)) {
			return "n{$delimiter}a";
		}
		return $string;
	}

	public function ascii_format($string) {
		if (function_exists('iconv')) {
			$lc_all = setlocale(LC_ALL, 0);
			setlocale(LC_ALL, $this->config()->get('locale'));
			$string = iconv(self::CHARSET, 'ASCII//TRANSLIT', (string)$string); // transliterate
			setlocale(LC_ALL, $lc_all);
		}
		return $string;
	}

	public function replace_non_ascii($subject, $replacement){
		return preg_replace('~[^\\pL\d]+~u', $replacement, $subject);
	}

	public function generate_tags_links(node_generic $node){
		$str='';
		if(!$node->get_auth('read')){
			return $str;
		}
		$tags=$this->replace_non_ascii(' ', $node->get('tags')); // replace non letter or digits by " "
		$tags=trim($tags, ' ');
		$tags=explode(' ', $tags);
		foreach($tags as $tag){
			if($tag != ' ' || $tag != ''){
				$str.=' <a href="?search='.urlencode($tag).'">'.$tag.'</a> ';
			}
		}
		return $str;
	}

	public function folder_view($nodes){
		$board=$this;
		include REAL_PATH.'includes/components/folder_view.php';
	}

	public function icons_view($nodes){
		$board=$this;
		include REAL_PATH.'includes/components/icons_view.php';
	}

	public function preview_view($nodes){
		$board=$this;
		include REAL_PATH.'includes/components/preview_view.php';
	}

	public function adaptable_view($nodes){
		if(count($nodes)<5){
			$this->preview_view($nodes);
		} elseif(count($nodes)<16){
			$this->icons_view($nodes);
		} else {
			$this->folder_view($nodes);
		}
	}

	public function generate_icon($action, $size=ICON_SMALL, $attributes=array()){
		$attributes=array_merge_recursive($attributes, array('class'=>'thumb'));
		if(is_array($action)){
			$action = array_shift($action);
		}
		$str='<span '.self::get_attributes_from_array($attributes).'><img src="'.HTTP_PATH.'images/actions/'.$action.'.png" alt="['.ucwords($this->config->localize($action)).']" width="'.$size.'" height="'.$size.'" /></span>';
		return $str;
	}

	public function generate_button($link, $action, $title=null, $size=ICON_SMALL, $attributes=array()){
		$attributes=array_merge_recursive($attributes, array('class'=>'button'));
		$str='<a href="'.$link.'" '.self::get_attributes_from_array($attributes).'>'.$this->generate_icon($action, $size).($title ? ' '.$title : '').'</a>';
		return $str;
	}

	public function generate_button_from_node($node, $size=ICON_SMALL, $action='', $attributes=array()){
		$attributes=array_merge_recursive($attributes, array('class'=>'button'));
		$str='<a href="'.$node->link($action).'" '.self::get_attributes_from_array($attributes).'>'.$node->get_html_thumb($size).' '.$this->shorten($node).'</a>';
		return $str;
	}

	public function generate_action_button_from_node($node, $action, $title=null, $size=ICON_SMALL, $attributes=array()){
		if($title === null){
			$title=ucwords($this->config->localize($action));
		}
		return $this->generate_button($node->link($action), $action, $this->localize($title), $size, $attributes);
	}

	public function generate_thumbnail($file, $size=ICON_SMALL, $title=''){
		$string='<span class="thumb"><img src="'.$file['url'].'" alt="['.$file['filename'].']" title="'.$title.'" width="'.$size.'" height="'.$size.'" /></span>';
		return $string;
	}

	public function convert_to_text($html,$cut=null){
		$string=html_entity_decode(strip_tags($html), ENT_COMPAT, 'UTF-8');
		if($cut==null){
			return $string;
		}
		return $this->summarize($string, $cut);
	}

	public static final function get_attributes_from_array(array $array){
		$str='';
		foreach($array as $key=>$value){
			if(is_array($value)){
				$value=implode(' ', $value);
			}
			$str.=$key.'="'.$value.'" ';
		}
		return $str;
	}

	public static function display_size($size){
	$units=array('o', 'Ko', 'Mo', 'Go', 'To', 'Po');
	$i=0;
	while($size > 1){
		$size=$size/1024;
		$i++;
	}
	return (ceil($size*102400)/100).' '.$units[$i-1];
}

	/**
	 * Here you can add the custom functions needed by your application.
	 */
}


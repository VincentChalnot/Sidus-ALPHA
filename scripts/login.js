var hexcase=0;
var b64pad="";
var chrsz=8;

function hex_sha1(s){return binb2hex(core_sha1(str2binb(s),s.length * chrsz));}

function core_sha1(x,f){x[f>>5]|=0x80<<(24-f%32);x[((f+64>>9)<<4)+15]=f;var w=Array(80);var a=1732584193;var b=-271733879;var c=-1732584194;var d=271733878;var e=-1009589776;for(var i=0;i<x.length;i+=16){var g=a;var h=b;var k=c;var l=d;var m=e;for(var j=0;j<80;j++){if(j<16)w[j]=x[i+j];else w[j]=rol(w[j-3]^w[j-8]^w[j-14]^w[j-16],1);var t=safe_add(safe_add(rol(a,5),sha1_ft(j,b,c,d)),safe_add(safe_add(e,w[j]),sha1_kt(j)));e=d;d=c;c=rol(b,30);b=a;a=t}a=safe_add(a,g);b=safe_add(b,h);c=safe_add(c,k);d=safe_add(d,l);e=safe_add(e,m)}return Array(a,b,c,d,e)}function sha1_ft(t,b,c,d){if(t<20)return(b&c)|((~b)&d);if(t<40)return b^c^d;if(t<60)return(b&c)|(b&d)|(c&d);return b^c^d}function sha1_kt(t){return(t<20)?1518500249:(t<40)?1859775393:(t<60)?-1894007588:-899497514}function safe_add(x,y){var a=(x&0xFFFF)+(y&0xFFFF);var b=(x>>16)+(y>>16)+(a>>16);return(b<<16)|(a&0xFFFF)}function rol(a,b){return(a<<b)|(a>>>(32-b))}function str2binb(a){var b=Array();var c=(1<<chrsz)-1;for(var i=0;i<a.length*chrsz;i+=chrsz)b[i>>5]|=(a.charCodeAt(i/chrsz)&c)<<(32-chrsz-i%32);return b}function binb2hex(a){var b=hexcase?"0123456789ABCDEF":"0123456789abcdef";var c="";for(var i=0;i<a.length*4;i++){c+=b.charAt((a[i>>2]>>((3-i%4)*8+4))&0xF)+b.charAt((a[i>>2]>>((3-i%4)*8))&0xF)}return c}

//COOKIE MANIPULATION
function getcookie(c_name){
if(document.cookie.length>0){
	c_start=document.cookie.indexOf(c_name+"=");
	if(c_start!=-1){ 
		c_start=c_start + c_name.length+1; 
		c_end=document.cookie.indexOf(";",c_start);
		if(c_end==-1) c_end=document.cookie.length;
			return unescape(document.cookie.substring(c_start,c_end));
		} 
	}
	return "";
}

function checkpass(){
	var t1=document.getElementById('temp1');
	var t2=document.getElementById('temp2');
	var sha=document.getElementById('sha1');
	var err=document.getElementById('passwd_error');
	if(t1.value==t2.value){
	  if(t1.value==''){
			t1.className+=' form_error';
			t2.className+=' form_error';
			err.innerHTML='Password can\'t be empty.';
			return false;
		}
		sha1.value=hex_sha1(t1.value);
		t1.value='';
		t2.value='';
		return true;
	} else {
		t1.value='';
		t2.value='';
		t1.className+=' form_error';
		t2.className+=' form_error';
		err.innerHTML='Passwords doesn\'t match.';
		return false;
	}
}

function hash_passwd(){
	document.getElementById('passwd_hashed').value=hex_sha1(document.getElementById('passwd_clear').value);
	document.getElementById('passwd_clear').value='';
	return true;
}

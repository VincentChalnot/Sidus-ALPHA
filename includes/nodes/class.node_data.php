<?php

require_once REAL_PATH.'includes/nodes/class.node_generic.php';

class node_data extends node_generic{

	protected $data=array();

	public function __construct(sys_config $config, $id){
		parent::__construct($config, $id);
		$query='SELECT data_type_name, data_label, data_value FROM node_data WHERE node_id='.$this->infos['node_id'];
		$tmp=$this->config->db()->get_array($query);
		foreach($tmp as $data){
			$this->data[$data['data_label']]=$this->config->convert($data['data_value'], $data['data_type_name']);
		}
	}

	public function get_data($key){
		if(isset($this->data[$key])){
			return $this->data[$key];
		}
		return false;
	}

	public function is_data_set($key){
		if(isset($this->data[$key])){
			return true;
		}
		return false;
	}

	public function get_all_data(){
		return $this->data;
	}

	protected function delete_more(){
		$query='DELETE FROM node_data WHERE node_id='.$this->infos['node_id'];
		if(!$this->config->db()->exec($query)){
			//TODO: Throw some error
			return false;
		}
		return true;
	}

	/**
	 * TODO !!!!!
	 *
	 */
	protected function edit_more($form){
		//TODO !!!
		return true;
	}

	public function set_data($key, $value){
		//TODO !!
		return true;
		
	}

}

<?php

require_once 'class.ui_input.php';

class ui_hidden extends ui_input {

	protected $type = 'hidden'; //HTML type (text, password, hidden)

	public function __construct($name, $default_value = null, $not_null = null, $not_used = false) {
		$this->name = $name;
		$this->not_null = $not_null;
		$this->default_value = $default_value;
	}

}

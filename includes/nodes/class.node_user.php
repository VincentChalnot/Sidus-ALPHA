<?php

require_once REAL_PATH.'includes/nodes/class.node_data.php';

class node_user extends node_data{

	protected $connected=false;
 //Define if a user is connected
	protected $groups=null;
	protected $public=array('node_id', 'parent_node_id', 'username');
	protected $friends=null;

	public function __construct(sys_config $config, $id=null){
		$this->config=$config; //Get handler from the current Database
		
		if($id != null){//In this case, the node is just called as an normal node (like a home folder)
			parent::__construct($config, $id);
			return;
		}
		
		if(!$this->config->session()->exists()){//If the user is NOT connected.
			//LOG UNEXISTING USER
			$this->infos=array('node_id'=>null, 'parent_node_id'=>3, 'user_id'=>null, 'username'=>$_SERVER['REMOTE_ADDR']);
			$this->auths=array('read'=>true, 'add'=>false, 'edit'=>false, 'delete'=>false);
			return;
		}
		
		//In any other case, the user is connected, so we need to construct the node accordingly
		parent::__construct($config, $this->config->session()->get('user_id'));
		$this->connected=true; //Define the user as connected
		$this->auths=array('read'=>true, 'add'=>true, 'edit'=>true, 'delete'=>true, 'ownership'=>true, 'mastership'=>true);//This is the user's own node
		return;
	}
	
	public function __toString(){
		if(!$this->infos['node_id']){
			return $this->config->localize('Anonymous');
		}
		if($this->auths['read']){
			return parent::__toString();
		}
		return (string)$this->get('username', false);
	}

	protected function get_more(){
		if(!$this->is_connected()){
			$query='SELECT username'.($this->is_owner($this->config->user())?',passwd,email':'').' FROM node_user WHERE user_id='.$this->infos['node_id'];
			$this->infos=array_merge($this->infos, (array)$this->config->db()->get_row($query));
			return;
		}
		parent::get_more();
		//Adding all node_user properties
		$query='SELECT * FROM node_user WHERE user_id='.$this->infos['node_id'];
		$this->infos=array_merge($this->infos, $this->config->db()->get_row($query));
	}
	
	protected static function add_more($id, sys_config $config, node_generic $parent_node, $form){
		parent::add_more($id, $config, $parent_node, $form);
		
		$username = $form->get_value('username');
		$passwd = $form->get_value('passwd');
		$passwd2 = $form->get_value('passwd2');
		$email = $form->get_value('email');

		if($passwd !== $passwd2){
			$config->error()->add(35);
			return false;
		}
		
		if(strlen($username) < 3){
			$config->error()->add(23);
			return false;
		}
		if($passwd == ''){
			$config->error()->add(24);
			return false;
		}
		$passwd=sha1($passwd);
		if(in_array(strtolower($username), array('root','admin','administrator','administrateur','moderator','moderateur'))){
			$config->error()->add(10);
			return false;
		}
		$query='SELECT COUNT(user_id) FROM node_user WHERE username=\''.$config->secure_input($username).'\'';
		$exists=(int)$config->db()->get_single($query);
		if($exists != 0){//Test if username already exists
			$config->error()->add(10);
			return false;
		}
		
		$query='INSERT INTO node_user (user_id,username,passwd,email) VALUES ('.$id.',\''.$config->secure_input($username).'\', \''.$config->secure_input($passwd).'\', \''.$config->secure_input($email).'\')';
		if(!$config->db()->exec($query)){
			$config->error()->add(30);
			return false;
		}

		$query = 'UPDATE node_generic SET inherit_permissions=0 WHERE node_id='.$id;
		if(!$config->db()->exec($query)){
			$config->error()->add(30, 3);
			return false;
		}
		
		$query='INSERT INTO node_permission (entity_id,node_id,b_ownership,b_mastership) VALUES ('.$id.','.$id.',1,1)';
		if(!$config->db()->exec($query)){
			$config->error()->add(30, 3);
			return false;
		}
				
		return $id;
	}

	/**
	 *
	 * @param type $action
	 * @param type $method
	 * @return ui_form
	 */
	public static function get_add_form($action='',$method='post'){
		$form = parent::get_add_form();
		require_once REAL_PATH.'includes/interface/class.ui_email.php';
		$form->remove('content');
		$form->remove('tags');
		$form->get('title')->set_label('Full Name :');
		$form->get('type_name')->set_value('user');
		$form->add(new ui_input('username', 'Username :', 'text'));
		$form->add(new ui_email('email', 'Email :'));
		$form->add(new ui_input('passwd', 'Password :', 'password'));
		$form->add(new ui_input('passwd2', 'Retype :', 'password'));
		return $form;
	}
	
	protected function delete_more(){
		$tmp=parent::delete_more();
		if($tmp==false){
			return false;
		}
		$query='DELETE FROM node_permission WHERE entity_id='.$this->infos['node_id'];
		if(!$this->config->db()->exec($query)){
			return false;
		}
		$query='DELETE FROM sys_session WHERE user_id='.$this->infos['node_id'];
		if(!$this->config->db()->exec($query)){
			return false;
		}
		$query='DELETE FROM node_user WHERE user_id='.$this->infos['node_id'];
		if(!$this->config->db()->exec($query)){
			return false;
		}
		return true;
	}

	protected function init_more_form(){
		require_once REAL_PATH.'includes/interface/class.ui_email.php';
		$this->content_form->add(new ui_input('passwd', $this->config->localize('Old Password').' : ', 'text'));
		$this->content_form->add(new ui_input('sha1'));
		$this->content_form->add(new ui_email('email', $this->config->localize('Email').' : ', $this->get('email',false)));
		parent::init_more_form();
	}
	
	protected function edit_more($form){
		$tmp = parent::edit_more($form);
		if($tmp == false){
			return false;
		}

		//TODO: Edit user-infos
		return true;
	}

	public function get_data($key){
//		if($this->is_connected()){
//			if($this->is_data_set($key)){
//				return parent::get_data($key);
//			}
//		}
		return $this->config->get($key);
	}

	public function login($username, $sha1){//Log the user in
		$query='SELECT u.user_id,u.passwd FROM node_generic AS n, node_user AS u WHERE n.type_name=\'user\' AND u.username=\''.$this->config->secure_input($username).'\' AND u.user_id=n.node_id';
		$tmp=$this->config->db()->get_row($query);
		if($tmp !== false && isset($tmp['passwd'], $tmp['user_id'])){//Get user_id and password in Database
			if(sha1($sha1) == $tmp['passwd'] || sha1(sha1($sha1)) == $tmp['passwd']){//Compare the hashs, test also the password unencrypted
				$this->connected=true;
				$this->config->session()->create_session($tmp['user_id']);
				return $tmp['user_id'];
			} else{//If password does not match
				$this->config->error()->add(3, 5, 'Password incorrect for username: '.$username);
				$this->security();
				return false;
			}
		} else{//If no user
			$this->config->error()->add(3, 4, 'Trying to login with a non existing username: '.$username);
			$this->security();
			return false;
		}
	}

	public function logout(){
		setcookie('signature', '', time() - 1200000, '/'); //Destroy cookie
		$query='DELETE FROM sys_session WHERE session_id=\''.$this->config->session()->get('session_id').'\'';
		$this->config->db()->exec($query); //Destroy session
	}

	//This means the user is connected and therefore this node IS the user
	public function is_connected(){//Return boolean
		return $this->connected;
	}

	public function remove_session($id){
		$id=(int)$id;
		if($id != $this->config->session()->get('session_id')){
			$query='DELETE FROM sys_session WHERE session_id='.$id.' AND user_id='.$this->infos['node_id'];
			if($this->config->db()->exec($query)){
				return true;
			} else{
				$this->config->error()->add(4);
				return false;
			}
		} else{
			$this->config->error()->add(17);
			return false;
		}
	}

	/**
	 *
	 */
	public function get_subscriptions(){
		if(!$this->connected){
			return array();
		}
		if($this->groups == null){
			$query='SELECT group_id FROM user_subscription WHERE user_id='.$this->infos['node_id'];
			$this->groups=array();
			$tmp=$this->config->db()->get_array($query); //Load groups
			foreach($tmp as $value){
				$this->groups[]=$value['group_id'];
			}
		}
		return $this->groups;
	}

	public function is_in_group($id){
		return in_array($id,$this->get_subscriptions());
	}

	public function get_current_session(){
		if($this->connected){
			$query='SELECT * FROM sys_session WHERE user_id='.$this->infos['node_id'].' AND session_id='.$this->config->session()->get('session_id');
			return $this->config->db()->get_array($query);
		} else {
			$this->config->error()->add(21);
			return false;
		}
	}
	
	public function get_opened_sessions(){
		if($this->connected){
			$query='SELECT * FROM sys_session WHERE user_id='.$this->infos['node_id'].' ORDER BY expire';
			return $this->config->db()->get_array($query);
		} else {
			$this->config->error()->add(21);
			return false;
		}
	}

	public function count_opened_sessions(){
		if($this->connected){
			$query='SELECT COUNT(*) FROM sys_session WHERE user_id='.$this->infos['node_id'].' ORDER BY expire';
			$tmp=$this->config->db()->get_single($query);
			return $tmp;
		} else{
			$this->config->error()->add(21);
			return false;
		}
	}

	/**
	 * Allow user-side configuration
	 */
	public function get_config($key){
//		if($this->is_connected()){
//			return $this->get('key');
//		}
		return $this->config->get($key);
	}

	/**
	 * TODO: Check security parameters
	 *
	 */
	protected final function security(){
		
	}

	/**
	 * Deprecated for now
	 */
	public function reinit_passwd($username, $email){
		$query='SELECT user_id,username,passwd FROM node_user WHERE email=\''.$this->config->secure_input($email).'\' AND username=\''.$this->config->secure_input($username).'\'';
		$user=$this->config->db()->get_row($query); //Get user_id and password in Database
		if($user != false){
			$newpass=$this->generate_passwd();
			if(!mail($user['username'].' <'.$email.'>', 'New password request', $user['username'].', you just asked for a new password from the board: '.$board."\n".'Your new password is: '.$new_pass."\n".'If this request wasn\'t made by yourself, contact immediatly the administrator of the board.')){
				die('Error: Can\'t send the email');
			}
			$query='UPDATE node_user SET passwd=\''.sha1(sha1($new_pass)).'\' WHERE user_id='.$user['node_id'];
			$this->config->db()->exec($query);
			$this->add_error(7);
		} else{
			$this->add_error(6);
		}
	}
	
	protected function generate_passwd(){
		$new_pass='';
		$possible='23456789ABCDEFGHJKLMNPQRSTVWXYZabcdefghijkmnpqrstvwxyz';
		$i=0;
		while($i < 8){
			$char=substr($possible, mt_rand(0, strlen($possible) - 1), 1);
			$new_pass.=$char;
			$i++;
		}
		return $newpass;
	}

}

<?php
$node=$this->current_node();
if(!$node->get_auth('read')){
	$this->redirect($node->link());
}
header('Content-Type: application/xml');
echo '<?xml version="1.0" encoding="UTF-8"?>'."\n";
echo '<?xml-stylesheet type="text/css" href="'.PROJECT_HTTP_PATH.'css/feed.css"?>'."\n";
?>
<rss xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:dc="http://purl.org/dc/elements/1.1/" version="2.0">
	<channel>
		<title><?php echo $node ?></title>
		<link><?php echo $node->link() ?></link>
		<description><?php echo html_entity_decode(strip_tags($node->get('content')),ENT_NOQUOTES,'UTF-8') ?></description>
		<content:encoded><![CDATA[<?php echo $node->get_html_content() ?>]]></content:encoded>
		<language><?php echo $this->config->get('lang') ?></language>
		<?php $childs=$node->get_childs('modification','DESC',100);
		if(count($childs)>0){?>
		<lastBuildDate><?php echo $childs[0]->get('modification') ?></lastBuildDate>
		<?php } else { ?>
		<lastBuildDate><?php echo $node->get('modification') ?></lastBuildDate>
		<?php }
		foreach($childs as $child){
			if($child->get_auth('read')){?>
				<item>
					<title><?php echo $child ?></title>
					<link><?php echo $child->link() ?></link>
					<pubDate><?php echo $child->get_creation('r') ?></pubDate>
					<dc:creator><?php echo $child ?></dc:creator>
					<guid isPermaLink="true"><?php echo PROJECT_HTTP_PATH.'index.php?node_id='.$child->get('node_id') ?></guid>
					<description><![CDATA[<?php echo html_entity_decode(strip_tags($child->get('content')),ENT_NOQUOTES,'UTF-8') ?>]]></description>
					<content:encoded><![CDATA[<?php echo $child->get_html_content() ?>]]></content:encoded>
				</item><?php
			}
		} ?>
	</channel>
</rss>

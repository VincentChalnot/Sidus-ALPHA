<?php

/**
 * GENERAL TODO !
 * 	node_copy (parent_node_id)
 * 	db cleanup
 *
 * Note: This is the default structure of a node, if you want to create a new class,
 * you need to rewrite (if you want) the following functions:
 * __construct, get_more, add_more, delete_more, edit_more, init_more_form
 * (TODO: Complete the list)
 */
class node_generic {

	protected $config; //Alias to the configuration node that contains everything
	protected $auths; //Array with the authorizations informations
	protected $infos; //Node informations (node_id, title, etc...)
	protected $public = array('node_id', 'parent_node_id'); //Public keys for the get() method
	protected $single_error = array('read' => true, 'add' => true, 'edit' => true, 'delete' => true); //This is meant to prevent repeating the same error message
	protected $perms; //Array containing the permissions for this node
	protected $content_form; //The form to edit the content of the node
	protected $permissions_form; //The form to edit the permissions of the node
	protected $position_form;

	/**
	 * This function initialize the node, it gets the basic informations out of the database,
	 * then it checks the permissions and some minor stuff.
	 * You can rewrite this constructor
	 * Don't forget to use parent::__construct()
	 */
	public function __construct(sys_config $config, $id) {
		$this->config = $config; //Get handler from the current Database
		$id = (int) $id;

		//Query basic informations
		$query = 'SELECT * FROM node_generic WHERE node_id='.$id;
		$this->infos = $this->config->db()->get_row($query);

		if ($this->infos == false) {//If the node doesn't exists in the database
			trigger_error('ABoard : Node '.$id.' doesn\'t exists in database.', E_USER_ERROR);
			exit;
		}

		$this->auths = $this->get_authorizations(); //Getting all the authorizations
		//Check ownership on current node
		//
		//If the user has the ownership
		if ($this->auths['ownership']) {
			if (!$this->infos['inherit_permissions']) {//and the permissions are NOT inherited
				$this->auths['read'] = true;
				$this->auths['add'] = true;
				$this->auths['edit'] = true;
				$this->auths['delete'] = true;
			} else {//else this means the ownership is corrupted by heritage
				$this->auths['ownership'] = false;
				if ($this->config->user() != null) {//If user is already initialized
					if ($this->config->user()->is_connected()) {//if the user is connected
						if ($this->is_owner($this->config->user())) {//This means the user is really the owner of the current node
							$this->auths['ownership'] = true;
							$this->auths['read'] = true;
							$this->auths['add'] = true;
							$this->auths['edit'] = true;
							$this->auths['delete'] = true;
						}
					}
				}
			}
		}
	}

	/**
	 * This function checks the permissions on the node and return an array with
	 * the corresponding permissions.
	 * We never check the ownership because we need this function for recursive tasks
	 */
	public final function get_authorizations() {
		//If the permissions of the node are already loaded, then returns the cache.
		if ($this->auths != null) {
			return $this->auths;
		}

		//If this node has inherited permissions, return the parent node's permissions
		if ($this->infos['inherit_permissions']) {
			$auths = $this->get_parent()->get_authorizations();
			//If the user is not initialized, returns current permissions
			if ($this->config->user() == null) {
				return $auths;
			}
			//If the user is not connected, returns the permissions right now.
			if (!$this->config->user()->is_connected()) {
				return $auths;
			}
			//Checking for each permissions/subsriptions ONLY FOR ONWERSHIP OR MASTERSHIP (+positive filter)
			foreach ($this->get_permissions() as $perm) {
				//If user is in permission or in group's permission
				if ($this->config->user()->is_in_group($perm['entity_id']) || $perm['entity_id'] == $this->config->user()->get('node_id')) {
					if (!$perm['b_inverse']) {//If permissions are not inversed (meaning they are normal)
						//Checking individual permissions
						if ($perm['b_ownership']) {
							$auths['ownership'] = true;
						}
						if ($perm['b_mastership']) {
							$auths['mastership'] = true;
							$auths['read'] = true;
							$auths['add'] = true;
							$auths['edit'] = true;
							$auths['delete'] = true;
						}
					}
				}
			}
			return $auths; //Will return parent's permissions + ownership or mastership of current node if exists
		}

		//Let's start with everything to false by default.
		$auths = array('read' => false, 'add' => false, 'edit' => false, 'delete' => false, 'ownership' => false, 'mastership' => false); //Init values
		//
		//Get anonymous authorizations
		$auths['read'] = (bool) $this->infos['anonymous_read'];
		$auths['add'] = (bool) $this->infos['anonymous_add'];
		$auths['edit'] = (bool) $this->infos['anonymous_edit'];
		$auths['delete'] = (bool) $this->infos['anonymous_delete'];

		//If the user is not initialized, returns current permissions
		if ($this->config->user() == null) {
			return $auths;
		}
		//If the user is not connected, returns the permissions right now.
		if (!$this->config->user()->is_connected()) {
			return $auths;
		}

		//Checking for each permissions/subsriptions (+positive filter)
		foreach ($this->get_permissions() as $perm) {
			//If user is in permission or in group's permission
			if ($this->config->user()->is_in_group($perm['entity_id']) || $perm['entity_id'] == $this->config->user()->get('node_id')) {
				if (!$perm['b_inverse']) {//If permissions are not inversed (meaning they are normal)
					//Checking individual permissions
					if ($perm['b_read'])
						$auths['read'] = true;
					if ($perm['b_add'])
						$auths['add'] = true;
					if ($perm['b_edit'])
						$auths['edit'] = true;
					if ($perm['b_delete'])
						$auths['delete'] = true;
					if ($perm['b_ownership'])
						$auths['ownership'] = true;
					if ($perm['b_mastership']) {
						$auths['mastership'] = true;
					}
				}
			}
		}

		//Checking for each permissions/subsriptions (-NEGATIVE filter)
		foreach ($this->get_permissions() as $perm) {
			//If user is in permission or in group's permission
			if ($this->config->user()->is_in_group($perm['entity_id']) || $perm['entity_id'] == $this->config->user()->get('node_id')) {
				if ($perm['b_inverse']) {//If permissions ARE INVERSED
					//Checking individual permissions
					if ($perm['b_read'])
						$auths['read'] = false;
					if ($perm['b_add'])
						$auths['add'] = false;
					if ($perm['b_edit'])
						$auths['edit'] = false;
					if ($perm['b_delete'])
						$auths['delete'] = false;
					//Note: You can't cancel ownership and mastership
				}
			}
		}

		//For the parents, we check the mastership of the node, which is a "transmissive" property
		if (!$this->is_root()) {
			if ($this->get_parent()->get_auth('mastership')) {
				$auths['mastership'] = true;
			}
		}

		//In any case, if the user has the mastership, he has all rights
		if ($auths['mastership']) {
			$auths['read'] = true;
			$auths['add'] = true;
			$auths['edit'] = true;
			$auths['delete'] = true;
		}

		return $auths;
	}

	/**
	 * This function test if the given entity is an owner of the node
	 * returns boolean
	 */
	public final function is_owner($entity) {
		if ($this->get('node_id') == $entity->get('node_id')) {
			return true;
		}
		foreach ($this->get_permissions() as $perm) {
			if ($perm['entity_id'] == $entity->get('node_id') && $perm['b_ownership'] && !$perm['b_inverse']) {
				return true;
			}
		}
		return false;
	}

	/**
	 * This function is used to get the owner(s) of this node
	 * return an array of node
	 */
	public final function get_owners() {
		$owners = array();
		foreach ($this->get_permissions() as $perm) {
			if ($perm['b_ownership'] && !$perm['b_inverse']) {
				if ($perm['entity_id'] == $this->infos['node_id']) {//If the current node is self-owned (user node)
					$owners[] = $this;
				} else {
					$owners[] = $this->config->node($perm['entity_id']);
				}
			}
		}
		return $owners;
	}

	/**
	 * Get first owner of the node
	 */
	public final function get_owner() {
		$owners = $this->get_owners();
		if (count($owners) > 0) {
			return $owners[0];
		}
		return false;
	}

	public final function get_permissions() {
		//Get all permissions for this node.
		if ($this->perms == null) {
			$query = 'SELECT * FROM node_permission WHERE node_id='.(int) $this->infos['node_id'];
			$this->perms = $this->config->db()->get_array($query);
		}
		return $this->perms;
	}

	/**
	 * Recursive function to get the inherited node (for the permissions)
	 */
	public final function get_inherited_node() {
		if ($this->infos['inherit_permissions']) {
			return $this->get_parent()->get_inherited_node();
		}
		return $this;
	}

	public function __toString() {
		return (string) $this->get('title', false);
	}

	/**
	 * This function returns the value of a property for this node.
	 */
	public final function get($key, $throw_error = true) {
		if (!$this->auths['read'] && !in_array($key, $this->public)) {//If user cannot read and key not public
			if ($throw_error) {
				$this->add_read_error();
				return false;
			}
			//Theses values are specific to the case where you want to display something instead of an error
			if ($key == 'title') {
				return $this->config->localize('Forbidden');
			}
			if ($key == 'type_name') {
				return 'secured';
			}
			return false;
		}
		if (array_key_exists($key, $this->infos)) {//Test array key
			return $this->infos[$key];
		}

		//Adding all node_infos properties
		$query = 'SELECT * FROM node_info WHERE node_id='.$this->infos['node_id'];
		$this->infos = array_merge($this->infos, (array) $this->config->db()->get_row($query));

		//Adding more properties (extended class)
		$this->get_more();

		if (array_key_exists($key, $this->infos)) {//Test array key
			return $this->infos[$key];
		}
		if ($throw_error == true) {
			$this->config->error()->add(11, 2, 'Trying to get unexisting property "'.$key.'"');
		}
		return false;
	}

	/**
	 * You need to rewrite this function in your extended classes to add other
	 * table properties in the infos array.
	 */
	protected function get_more() {
		//And add your custom code here
	}

	/**
	 * Return a string containing the URL to the node
	 * @param <string> $options
	 * @return <string> URL
	 */
	public function link($options = null) {
		return $this->config->link($this, $options);
	}

	/**
	 * Returns a formated string with the creation date.
	 */
	public final function get_creation($format = DATE_DEFAULT) {
		$str = $this->get('creation');
		if ($str == false) {
			return false;
		}
		return $this->config->date()->date($str, $format);
	}

	/**
	 * Returns a formated string with the modification date.
	 */
	public final function get_modification($format = DATE_DEFAULT) {
		$str = $this->get('modification');
		if ($str == false) {
			return false;
		}
		return $this->config->date()->date($str, $format);
	}

	/**
	 * Returns a boolean with the permission for the key (read, add, edit, delete)
	 */
	public final function get_auth($key) {
		if (!array_key_exists($key, $this->auths)) {//Test array key
			$this->config->user()->add_error(11);
			return false;
		}
		return (bool) $this->auths[$key];
	}

	/**
	 * Add a new node of this type
	 */
	public final static function add_node(sys_config $config, node_generic $parent_node, ui_form $form) {
		if (!$parent_node->get_auth('add')) {
			$config->error()->add(13);
			return false;
		}
		if (!$form->validate()) {
			$config->error()->add(31);
			return false;
		}
		$type_name = $form->get('type_name')->get_value();
		$tmp = $parent_node->get_allowed_child_types();
		if (!in_array($type_name, $tmp)) {//If this type of node is permitted
			$config->error()->add(19);
			return false;
		}
		$config->db()->begin_transaction(); //Start transaction to enable rollback
		$query = "INSERT INTO node_generic (parent_node_id,type_name,title,creator,index_num) VALUES ({$parent_node->get('node_id')},'{$type_name}','{$config->secure_input($form->get_value('title'))}','{$config->user()}',{$parent_node->get_next_child_index()})";
		if (!$config->db()->exec($query)) {
			$config->error()->add(30, 3);
			$config->db()->rollback_transaction();
			return false;
		}
		$id = $config->db()->get_last_id();
		if ($id == false) {
			$config->error()->add(30, 3);
			$config->db()->rollback_transaction();
			return false;
		}
		$query = 'INSERT INTO node_info (node_id,creation,modification,content,tags) VALUES ('.$id.','.$config->date()->now().','.$config->date()->now().',\''.$config->secure_input($form->get_value('content'), true).'\',\''.$config->secure_input($form->get_value('tags')).'\')';
		if (!$config->db()->exec($query)) {
			$config->error()->add(30, 3);
			$config->db()->rollback_transaction();
			return false;
		}
		if ($config->user()->is_connected()) {
			$query = 'INSERT INTO node_permission (entity_id,node_id,b_ownership) VALUES ('.$config->user()->get('node_id').','.$id.',1)';
			if (!$config->db()->exec($query)) {
				$config->error()->add(30, 3);
				$config->db()->rollback_transaction();
				return false;
			}
		}
		require_once $config->get_class_path($type_name);
		$tmp = call_user_func(array($config->get_class_name($type_name), 'add_more'), $id, $config, $parent_node, $form);
		if (!$tmp) {
			$config->error()->add(30, 3);
			$config->db()->rollback_transaction();
			return false;
		}
		//Updating modification date for parent node
		$query = 'UPDATE node_info SET modification='.$config->date()->now().' WHERE node_id='.$parent_node->get('node_id');
		$config->db()->exec($query);
		$config->db()->commit_transaction();
		sys_config::get_instance()->node($id)->post_add($form);
		return (int) $id;
	}

	protected static function add_more($id, sys_config $config, node_generic $parent_node, $form) {
		//Add your functions here, be careful to return false if an error occured and true if everything worked.
		return true;
	}

	public static function get_add_form($action = '', $method = 'post') {
		require_once REAL_PATH.'includes/interface/class.ui_form.php';
		require_once REAL_PATH.'includes/interface/class.ui_input.php';
		require_once REAL_PATH.'includes/interface/class.ui_textarea.php';
		$form = new ui_form($action, $method);
		$form->set_prefix('add_');
		$form->add(new ui_input('type_name', null, 'hidden'));
		$form->add(new ui_input('title', 'Title :', 'text'));
		$form->add(new ui_textarea('content', 'Content :'));
		$form->add(new ui_input('tags', 'Tags :', 'text', ''));
		return $form;
	}

	public function post_add(ui_form $form){
		return true;
	}

	/**
	 * Returns the next free child index for the current node.
	 */
	public final function get_next_child_index() {
		$query = 'SELECT index_num FROM node_generic WHERE parent_node_id='.$this->infos['node_id'].' ORDER BY index_num DESC LIMIT 1';
		return (int) $this->config->db()->get_single($query) + 1;
	}

	/**
	 * Get the parent of the node
	 */
	public final function get_parent() {
		if ($this->is_root()) {
			return $this;
		}
		return $this->config->node($this->infos['parent_node_id']);
	}

	/**
	 * Return all parents in an array of node ordered from the nearest parent
	 * to the root node.
	 */
	public final function get_parents() {
		$result = array(); //Array of nodes
		if ($this->infos['node_id'] != $this->infos['parent_node_id']) {
			$tmp = $this;
			do {
				$tmp = $tmp->get_parent();
				$result[] = $tmp;
			} while ($tmp->get('node_id') != $tmp->get('parent_node_id'));
		}
		return $result;
	}

	/**
	 * Get the childs of the current node.
	 * You can specify options:
	 * [node property], asc|desc, (int) [limit], (Array) [allowed types]
	 */
	public final function get_childs($order_by = 'index_num', $order = 'ASC', $limit = null, $types = array()) {
		if (!$this->auths['read']) {
			$this->add_read_error();
			return false;
		}
		$order = strtoupper($order);
		$result = array();
		$or_conditions = array();
		$and_conditions = array();
		//Check if the ordering type matches
		$column = array('type_name', 'title', 'index_num', 'views', 'creation', 'modification');
		if (!in_array($order_by, $column)) {//If not a column name
			$order_by = 'index_num'; //Default column for ordering
			$this->config->error()->add(0); //TODO: Needs to throw some kind of error
		}
		if ($order != 'DESC') {//If not DESC
			$order = 'ASC'; //Then it must be ASC (default for SQL ordering)
		}
		foreach ($types as $type) {//For each type of node we want
			if (substr($type, 0, 1) == '-') {
				$type = substr($type, 1);
				if ($this->config->is_type($type)) {//If the type exists
					$and_conditions[] = 'type_name!=\''.$type.'\''; //We add a condition
				}
			} elseif ($this->config->is_type($type)) {//If the type exists
				$or_conditions[] = 'type_name=\''.$type.'\''; //We add a condition
			}
		}
		$query = 'SELECT n.node_id,'.$order_by.' FROM node_generic AS n, node_info AS i WHERE n.parent_node_id='.$this->infos['node_id'].' AND n.node_id!='.$this->infos['node_id'].' AND n.node_id=i.node_id ';
		if (count($or_conditions) > 0) {
			$query.=' AND ('.implode(' OR ', $or_conditions).')';
		}
		if (count($and_conditions) > 0) {
			$query.=' AND '.implode(' AND ', $and_conditions);
		}
		$query.=' ORDER BY '.$order_by.' '.$order;
		if ($limit != null) {
			$query.=' LIMIT '.(int) $limit;
		}
		$tmp = $this->config->db()->get_array($query);
		foreach ($tmp as $value) {
			$result[] = $this->config->node($value['node_id']);
		}
		return $result;
	}

	/**
	 * Get one child from the current node.
	 * You can specify options:
	 * [node property], asc|desc, (Array) [allowed types]
	 */
	public final function get_child($order_by = 'index_num', $order = 'ASC', $types = array()) {
		$childs = $this->get_childs($order_by, $order, 1, $types);
		return array_pop($childs);
	}


	/**
	 * Return the localized name of the type of this node.
	 */
	public final function get_type() {
		return $this->config->get_localized_type($this->get('type_name'));
	}

	/**
	 * Return an array with the allowed types for this node.
	 * This is probably not what you are looking for, check the next function.
	 * For futur use if we authorize node conversion
	 */
	public final function get_allowed_types() {
		if (!($this->auths['read'] || $this->auths['add'] || $this->auths['edit'])) {
			$this->add_read_error();
			return false;
		}
		$query = 'SELECT a.allowed_type FROM node_generic AS n, allowed_type AS a WHERE n.type_name=a.type_name AND n.node_id='.$this->infos['parent_node_id'];
		$tmp = $this->config->db()->get_array($query);
		$tmp2 = array();
		foreach ($tmp as $value) {
			$tmp2[] = $value['allowed_type'];
		}
		return $tmp2;
	}

	/**
	 * Return an array with the authorized types for the childs inside this node.
	 */
	public final function get_allowed_child_types() {
		if (!($this->auths['read'] || $this->auths['add'] || $this->auths['edit'])) {
			$this->add_read_error();
			return false;
		}
		$query = 'SELECT a.allowed_type FROM node_generic AS n, allowed_type AS a WHERE n.type_name=a.type_name AND n.node_id='.$this->infos['node_id'];
		$tmp = $this->config->db()->get_array($query);
		$tmp2 = array();
		foreach ($tmp as $value) {
			$tmp2[] = $value['allowed_type'];
		}
		return $tmp2;
	}

	/**
	 * TODO: Needs rewriting !
	 */
	public final function delete($transaction = true) {
		//Check permissions
		if (!$this->auths['delete']) {
			$this->add_del_error();
			return false;
		}

		//Some elements can't be deleted
		if ($this->is_root() || in_array($this->infos['type_name'], array('users', 'groups'))) {
			$this->config->error()->add(18);
			return false;
		}

		//This is a recursive call to delete all child nodes
		//If a node can't be deleted, then the process just stops.

		if ($transaction) {
			$this->config->db()->begin_transaction();
		}

		$tmp = $this->get_childs(); //Get all childs
		if (count($tmp) > 0) {//If there is any
			foreach ($tmp as $child) {//Try to delete each child
				if (!$child->delete(false)) {//Just continue on success
					$this->config->error()->error(22);
					if ($transaction) {
						$this->config->db()->rollback_transaction();
					}
					return false;
				}
			}
		}
		$query = 'DELETE FROM node_permission WHERE node_id='.$this->infos['node_id'];
		if (!$this->config->db()->exec($query)) {
			$this->config->error()->add(0); //TODO: Throw some error
			if ($transaction) {
				$this->config->db()->rollback_transaction();
			}
			return false;
		}
		$query = 'DELETE FROM node_info WHERE node_id='.$this->infos['node_id'];
		if (!$this->config->db()->exec($query)) {
			$this->config->error()->add(0); //TODO: Throw some error
			if ($transaction) {
				$this->config->db()->rollback_transaction();
			}
			return false;
		}
		if ($this->delete_more() == false) {
			$this->config->error()->add(0); //TODO: Throw some error
			if ($transaction) {
				$this->config->db()->rollback_transaction();
			}
			return false;
		}
		$query = 'DELETE FROM node_generic WHERE node_id='.$this->infos['node_id'];
		if (!$this->config->db()->exec($query)) {
			$this->config->error()->add(0); //TODO: Throw some error
			if ($transaction) {
				$this->config->db()->rollback_transaction();
			}
			return false;
		}
		if ($transaction) {
			$this->config->db()->commit_transaction();
		}
		//Updating modification date for parent
		$query = 'UPDATE node_info SET modification='.$this->config->date()->now().' WHERE node_id='.$this->infos['parent_node_id'];
		$this->config->db()->exec($query);
		return true;
	}

	/**
	 * You might want to delete other stuff for specific nodes. Add your queries in here.
	 */
	protected function delete_more() {
		//Add your functions here, be careful to return false if an error occured and true if everything worked.
		return true;
	}

	/**
	 * This function is called by the controller if some inputs needs to be passed
	 * to the form registred for this node.
	 */
	public final function edit($form = null) {
		if (!$this->auths['edit']) {//Test permissions
			$this->add_edit_error();
			return false;
		}
		if ($form == null) {
			$form = $this->form();
		} else {
			if (is_string($form)) {
				$form = $this->form($form);
			}
		}
		if (!$form->validate()) {
			$this->config->error()->add(31);
			return false;
		}

		$list = array();
		if ($form->get('title')) {
			$list[] = 'title=\''.$this->config->secure_input($form->get_value('title')).'\'';
		}
		if ($form->get('index_num')) {
			$list[] = 'index_num='.(int) $form->get_value('index_num');
		}
		if ($form->get('parent_node_id')) {
			$list[] = 'parent_node_id='.(int) $form->get_value('parent_node_id');
		}
		if ($form->get('override_view') && $this->get_auth('mastership')) {
			$list[] = 'override_view=\''.$this->config->secure_input($form->get_value('override_view')).'\'';
		}
		foreach (array('read', 'add', 'edit', 'delete') as $p) {
			if ($form->get('anonymous.'.$p)) {
				$list[] = 'anonymous_'.$p.'='.(int) (bool) $form->get_value('anonymous.'.$p);
			}
		}
		if (!$this->is_root() && $form->get('inherit_permissions')) {
			$list[] = 'inherit_permissions='.(int) (bool) $form->get_value('inherit_permissions');
		}
		if (count($list) > 0) {
			$query = 'UPDATE node_generic SET '.implode(', ', $list).' WHERE node_id='.$this->infos['node_id'];
			if (!$this->config->db()->exec($query)) {
				$this->config->error()->add(0); //TODO : Throw some error
				return false;
			}
		}

		//Save all changes on acive permissions
		foreach ($this->get_permissions() as $permission) {
			$list = array();
			foreach (array('read', 'add', 'edit', 'delete', 'ownership', 'mastership', 'inverse') as $perm) {
				if ($form->get('entity_'.$permission['entity_id'].'.'.$perm)) {
					$list[] = 'b_'.$perm.'='.(int) (bool) $form->get_value('entity_'.$permission['entity_id'].'.'.$perm);
				}
			}
			if (count($list) > 0) {
				$query = 'UPDATE node_permission SET '.implode(', ', $list).' WHERE node_id='.$this->infos['node_id'].' AND entity_id='.$permission['entity_id'];
				if (!$this->config->db()->exec($query)) {
					$this->config->error()->add(0); //TODO : Throw some error
					return false;
				}
			}
		}

		/**
		 * ARGH! Can't find another way to do this !!!
		 */
		if (isset($_POST['add_new_entity'])) {
			$query = 'INSERT INTO node_permission (entity_id,node_id,b_read,b_add,b_edit,b_delete,b_ownership,b_mastership,b_inverse) VALUES (';
			$query.=(int) $form->get_value('new_entity_id').','.$this->infos['node_id'];
			foreach (array('read', 'add', 'edit', 'delete', 'ownership', 'mastership', 'inverse') as $perm) {
				$query.=','.(int) $form->get_value('new.'.$perm);
			}
			$query.=')';
			if (!$this->config->db()->exec($query)) {
				$this->config->error()->add(0); //TODO:Throw some error
			}
		}

		//Editing node_info
		$list = array();
		if ($form->get('content')) {
			$list[] = 'content=\''.$this->config->secure_input($form->get_value('content'), true).'\'';
		}
		if ($form->get('tags')) {
			$list[] = 'tags=\''.$this->config->secure_input($form->get_value('tags')).'\'';
		}
		if ($form->get('modification')) {
			$list[] = 'modification='.$this->config->date()->now();
		}
		if (count($list) > 0) {
			$query = 'UPDATE node_info SET '.implode(', ', $list).' WHERE node_id='.$this->infos['node_id'];
			if (!$this->config->db()->exec($query)) {
				$this->config->error()->add(0);
				return false;
			}
		}

		//Editing more fields (extended class)
		if (!$this->edit_more($form)) {
			$this->config->error()->add(0); //TODO : Throw some error
			return false;
		}

		return true;
	}

	/**
	 * Edit more: That's where you want to put your custom tables.
	 */
	protected function edit_more($form) {
		//Add your functions here, be careful to return false if an error occured and true if everything worked.
		return true;
	}

	public function remove_permission($id) {
		$query = 'DELETE FROM node_permission WHERE node_id='.$this->infos['node_id'].' AND entity_id='.(int) $id;
		if (!$this->config->db()->exec($query)) {
			$this->config->error()->add(0); //TODO : Throw some error
			return false;
		}
		return true;
	}

	/**
	 * Initialize all basic fields for node edition and then call init_more_fields
	 * where you can add your customs inputs.
	 */
	protected final function init_content_form() {
		if (!$this->auths['edit']) {
			$this->add_edit_error();
			return false;
		}
		//Form initialization
		require_once REAL_PATH.'includes/interface/class.ui_form.php';
		$this->content_form = new ui_form();
		//Prefix used for all the inputs names to avoid duplicate ids
		$this->content_form->set_prefix('n'.$this->infos['node_id'].'_'); //Don't change this !!!
		//Basic HTML inputs for most of the fields
		require_once REAL_PATH.'includes/interface/class.ui_input.php';
		require_once REAL_PATH.'includes/interface/class.ui_textarea.php';

		/**
		 * The following lines are all the basics information a node should contain
		 */
		$this->content_form->add(new ui_input('title', $this->config->localize('Title').' : ', 'text', $this->get('title'), true));
		$this->content_form->add(new ui_textarea('content', 'Content :', false, $this->get('content')));
		$this->content_form->add(new ui_input('tags', 'Tags : ', 'text', $this->get('tags')));
        $this->content_form->add(new ui_input('index_num', 'Index : ', 'text', $this->get('index_num')));
		if($this->get_auth('mastership')){
			$this->content_form->add(new ui_input('override_view', 'Override View : ', 'text', $this->get('override_view')));
		}

		//Call the function for extended classes.
		$this->init_more_form();
		return $this->content_form;
	}

	/**
	 * Initialize all basic fields for node edition and then call init_more_fields
	 * where you can add your customs inputs.
	 */
	protected final function init_permissions_form() {
		if (!$this->auths['edit']) {
			$this->add_edit_error();
			return false;
		}
		if (!($this->auths['ownership'] || $this->auths['mastership'])) {//The user can't modify permissions if he doesn't have ownership or mastership
			$this->config->error()->add(0); //TODO !!!!
			return false;
		}
		//Form initialization
		require_once REAL_PATH.'includes/interface/class.ui_form.php';
		$this->permissions_form = new ui_form();
		//Prefix used for all the inputs names to avoid duplicate ids
		$this->permissions_form->set_prefix('n'.$this->infos['node_id'].'_'); //Don't change this !!!
		//Basic HTML inputs for most of the fields
		require_once REAL_PATH.'includes/interface/class.ui_input.php';
		require_once REAL_PATH.'includes/interface/class.ui_checkbox.php';
		require_once REAL_PATH.'includes/interface/class.ui_select.php';

		/**
		 * The following lines are all the basics information a node should contain
		 */
		$this->permissions_form->add(new ui_checkbox('inherit_permissions', $this->config->localize('Inherit permissions').' : ', (int) $this->get('inherit_permissions')));
        if ($this->get('inherit_permissions')) {
            return $this->permissions_form;
        }
		$this->permissions_form->add(new ui_checkbox('anonymous.read', null, (int) $this->get('anonymous_read')));
		$this->permissions_form->add(new ui_checkbox('anonymous.add', null, (int) $this->get('anonymous_add')));
		$this->permissions_form->add(new ui_checkbox('anonymous.edit', null, (int) $this->get('anonymous_edit')));
		$this->permissions_form->add(new ui_checkbox('anonymous.delete', null, (int) $this->get('anonymous_delete')));

		/*
		 * Already registered entities
		 */
		$active_entities = array();
		foreach ($this->get_permissions() as $permission) {
			$label = $permission['entity_id'].' - '.$this->config->node($permission['entity_id']);
			foreach (array('read', 'add', 'edit', 'delete', 'ownership', 'mastership', 'inverse') as $perm) {
				$this->permissions_form->add(new ui_checkbox('entity_'.$permission['entity_id'].'.'.$perm, null, $permission['b_'.$perm]));
			}
			$active_entities[$permission['entity_id']] = $label;
		}

		/*
		 * Looking for other entities not yet in the permissions and propose to add them.
		 */
		$query = 'SELECT node_id FROM node_generic WHERE type_name=\'user\' OR type_name=\'group\'';
		$ids = $this->config->db()->get_array($query);
		$inactive_entities = array();
		foreach ($ids as $id) {
			if (!array_key_exists($id['node_id'], $active_entities)) {
				$inactive_entities[$id['node_id']] = $id['node_id'].' - '.$this->config->node($id['node_id']);
			}
		}
		if (count($inactive_entities) > 0) {
			$this->permissions_form->add(new ui_select('new_entity_id', $this->config->localize('New').' : ', $inactive_entities));
			foreach (array('read', 'add', 'edit', 'delete', 'ownership', 'mastership', 'inverse') as $perm) {
				$this->permissions_form->add(new ui_checkbox('new.'.$perm, null));
			}
		}
		return $this->permissions_form;
	}

	/**
	 * Initialize all basic fields for node edition and then call init_more_fields
	 * where you can add your customs inputs.
	 */
	protected final function init_position_form() {
		if (!$this->auths['edit']) {
			$this->add_edit_error();
			return false;
		}
		//Form initialization
		require_once REAL_PATH.'includes/interface/class.ui_form.php';
		$this->position_form = new ui_form();
		//Prefix used for all the inputs names to avoid duplicate ids
		$this->position_form->set_prefix('n'.$this->infos['node_id'].'_'); //Don't change this !!!
		//Basic HTML inputs for most of the fields
		require_once REAL_PATH.'includes/interface/class.ui_input.php';
		require_once REAL_PATH.'includes/interface/class.ui_select.php';

		$possible_parent_types = [];
		$query = "SELECT type_name FROM allowed_type WHERE allowed_type='{$this->get('type_name')}'";
		foreach ($this->config->db()->get_array($query) as $value) {
			$possible_parent_types[] = "'" . $this->config->secure_input($value['type_name']) . "'";
		}
		$type_list = implode($possible_parent_types, ', ');
		$possible_parents = [];
		$query = "SELECT node_id, title FROM node_generic WHERE type_name IN ({$type_list})";
		foreach ($this->config->db()->get_array($query) as $value) {
			$possible_parents[$value['node_id']] = $value['title'];
		}

		$this->position_form->add(new ui_select('parent_node_id', 'Parent node : ', $possible_parents, $this->get('parent_node_id')));
		return $this->position_form;
	}

	/**
	 * Add your customs user inputs in this function for CONTENT ONLY
	 */
	protected function init_more_form() {
		//Don't forget to prefix your field's name !
		//Add your custom fields here
	}

	/**
	 * Return the form object
	 */
	public final function form($key = 'content') {
		switch ($key) {
			case 'content':
				if ($this->content_form != null) {
					return $this->content_form;
				}
				return $this->init_content_form();
			case 'permissions':
				if ($this->permissions_form != null) {
					return $this->permissions_form;
				}
				return $this->init_permissions_form();
			case 'location':
				if ($this->position_form != null) {
					return $this->position_form;
				}
				return $this->init_position_form();
			default:
				return false;
		}
	}

	/**
	 * Test if the node is a root node
	 */
	public final function is_root() {
		return $this->infos['node_id'] == $this->infos['parent_node_id'];
	}

	/**
	 * Add a view to the current node
	 */
	public final function add_view() {
		if (!$this->auths['read']) {
			$this->add_read_error();
			return false;
		}
		$query = 'UPDATE node_info SET views=views+1 WHERE node_id='.$this->infos['node_id'];
		$this->config->db()->exec($query);
	}

	//READ/ADD/EDIT/DEL ERRORS
	//These function are meant to prevent the call of two time the same error message.
	protected final function add_read_error() {
		if ($this->single_error['read']) {
			$this->config->error()->add(12);
		}
		$this->single_error['read'] = false;
	}

	protected final function add_add_error() {
		if ($this->single_error['add']) {
			$this->config->error()->add(13);
		}
		$this->single_error['add'] = false;
	}

	protected final function add_edit_error() {
		if ($this->single_error['edit']) {
			$this->config->error()->add(14);
		}
		$this->single_error['edit'] = false;
	}

	protected final function add_del_error() {
		if ($this->single_error['delete']) {
			$this->config->error()->add(15);
		}
		$this->single_error['delete'] = false;
	}

	//Everything beyond that line is used to generate content

	public function button($size = ICON_SMALL, $action = '', $attributes = array()) {
		return $this->config->board()->generate_button_from_node($this, $size, $action, $attributes);
	}

	public function action_button($action, $title = null, $size = ICON_SMALL, $attributes = array()) {
		return $this->config->board()->generate_action_button_from_node($this, $action, $title, $size, $attributes);
	}

	/**
	 * Getting the thumbnail
	 */
	public function get_thumb() {
		if ($this->get_auth('read')) {
			$icons = $this->get_childs('index_num', 'ASC', null, array('icon'));
			if (count($icons) > 0) {//If there are icons
				foreach ($icons as $icon) {//We take the first one we are authorized to see
					if ($icon->get_auth('read')) {
						return $icon->get_thumb(); //Return the file if found
					}
				}
			}
		}
		//Else return default icon for type of node
		return $this->config->get_thumb($this->get('type_name', false));
	}

	public function get_html_thumb($size = ICON_SMALL) {
		if ($this->get_auth('read')) {
			return $this->config->board()->generate_thumbnail($this->get_thumb(), $size, $this);
		}
		return $this->config->board()->generate_thumbnail($this->config->get_thumb('secured'), $size, $this);
	}

	public function get_html_content() {
		if (!$this->auths['read']) {//Test permissions
			$this->add_read_error();
			return false;
		}
		$string = $this->get('content');
		return $string;
	}

}

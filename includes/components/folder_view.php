<?php if(!isset($nodes, $board)){trigger_error('You must set $nodes and $board before loading the following component :'.$_SERVER['SCRIPT_NAME']);return;} ?>
<table class="folder_view">
	<tbody>
		<tr class="folder_header">
			<th class="a"><?php echo $board->config->localize('Title') ?></th>
			<th class="b"><?php echo $board->config->localize('Type') ?></th>
			<th class="c"><?php echo $board->config->localize('Owner') ?></th>
			<th class="d"><?php echo $board->config->localize('Creation') ?></th>
			<th class="e"><?php echo $board->config->localize('Modification') ?></th>
		</tr>
		<?php if($nodes == false): ?>
			<tr>
				<td colspan="5" class="empty"><?php echo $board->config->localize('Empty') ?></td>
			</tr>
		<?php else: ?>
			<?php $odd='' ?>
			<?php foreach($nodes as $child): ?>
				<?php if($child->get_auth('read')): ?>
					<tr class="<?php echo $odd ?>">
						<td class="a" title="<?php echo $child ?>">
							<a href="<?php echo $child->link() ?>"><?php echo $child->get_html_thumb(ICON_SMALL).' '.$board->shorten($child, 50) ?></a>
						</td>
						<td class="b" title="<?php echo $child->get_type() ?>">
							<?php echo ucwords($board->shorten($child->get_type(), 12)) ?>
						</td>
						<?php if($child->get_owner() == false): ?>
							<td class="c"><?php echo $board->config->localize('Nobody') ?></td>
						<?php else: ?>
							<td class="c"><?php echo $child->get_owner() ?></td>
						<?php endif ?>
						<td class="d" title="<?php echo $child->get_creation() ?>"><?php echo $child->get_creation(DATE_TINY) ?></td>
						<td class="e" title="<?php echo $child->get_modification() ?>"><?php echo $child->get_modification(DATE_TINY) ?></td>
					</tr>
					<?php $odd=($odd=='')?' odd':'' ?>
				<?php endif ?>
			<?php endforeach ?>
		<?php endif; ?>
	</tbody>
</table>
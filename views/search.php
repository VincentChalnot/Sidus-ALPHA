<?php

$search=$board->secure_display($_GET['search']);
if(strlen($search)<3){
	echo '<div class="header">Le sujet doit faire au moins 3 caractères</div>';
  unset($search);
} else {
  echo '<div class="header">Recherche de "'.$search.'" dans la base de donnée...</div>';
}

echo '<div id="content">';

if(isset($search)){
  $no_result=true;
  $tmp=$board->search($search,'title');
  if(count($tmp)>0 && $tmp!=false){
	$no_result=false;
	  echo '<div class="separator">"'.$search.'" dans les titres :</div>';
		$board->folder_view($tmp);
	}

	$tmp=$board->search($search,'tags');
  if(count($tmp)>0 && $tmp!=false){
	$no_result=false;
	  echo '<div class="separator">"'.$search.'" dans les mots clés :</div>';
		$board->folder_view($tmp);
	}

	$tmp=$board->search($search,'content');
  if(count($tmp)>0 && $tmp!=false){
	$no_result=false;
	  echo '<div class="separator">"'.$search.'" dans le contenu :</div>';
		$board->folder_view($tmp);
	}
	
	$tmp=$board->search($search,'username');
  if(count($tmp)>0 && $tmp!=false){
	$no_result=false;
	  echo '<div class="separator">"'.$search.'" dans les utilisateurs  :</div>';
		$board->folder_view($tmp);
	}

	if($no_result){
		echo '<div class="separator">Aucun résultat pour "'.$search.'" dans la base de données</div>';
	}

}
echo '</div>';

<?php

/**
 * This is the abstraction class for your controller class.
 * TODO: Rewrite this definition
 * The controller is handling all actions from the user.
 * @author Vincent Chalnot
 * @version 1.0.1
 */
//define('DEFAULT_NODE_ID', 1);//Define your default node id

class proto_controller{

	protected $config; //class.sys_config.php
	protected $current_node_id; //This is the node id loaded by the application
	protected $view_path; //Current view path (absolute)
	protected $debug=false;
	protected $viewname='read';
	protected $form;

	/*
	 * Here you can override these settings to globally disallow
	 * some actions in your application.
	 */
	protected $allow_add=true;
	protected $allow_edit=true;
	protected $allow_delete=true;
	protected $allow_download=true;

	public final function __construct(sys_config $config){
		$this->config=$config;
		$this->init_controller();
	}

	/**
	 * Controller initialization, this allow the basic actions of the client
	 * to be taken into account, such as login in, login out, managing sessions
	 * and most importantly: managing nodes.
	 */
	protected function init_controller(){
		$this->pre_load();
		$this->init_application();
	}

	/**
	 * Here we only take care of the actions previous to the loading
	 * of the application such as login in, login out an removing a session.
	 */
	protected function pre_load(){
		if($this->config->user()->is_connected()){
			if(isset($_GET['logout'])){
				$this->logout();
			}
			if(isset($_GET['del_session'])){
				$this->remove_session($_GET['del_session']);
			}
		} else {
			if(isset($_GET['subscribe'])){
				$this->form = node_user::get_add_form('subscribe');
				$this->subscribe();
			}
			if(isset($_POST['login'], $_POST['username'], $_POST['password'])){
				$this->login($_POST['username'], $_POST['password']);
			}
		}
	}

	/**
	 * A generic way of handling the application
	 * @return <type>
	 */
	protected function init_application(){
		//You need to handle specific actions: meaning every action that is not linked to a node.
		//Usually, you only want to associate a specific view to an action.

		//From here, you only want to take the nodes into account
		if(isset($_GET['node_id'])){//If node_id isn't set, set default
			$this->current_node_id=(int)$_GET['node_id'];
		} else{
			$this->current_node_id=DEFAULT_NODE_ID;
		}

		if($this->config->node($this->current_node_id) == false){//If the node doesn't exists.
			$this->redirect($this->config->node(DEFAULT_NODE_ID)->link(array('error'=>'404')));
		}

		$type=$this->current_node()->get('type_name');
		$function='load_'.$type;

		if(method_exists($this, $function)){
			$this->$function();
		} else{
			$this->load_generic();
		}

		if(isset($_GET['admin'])){
			$this->redirect(PROJECT_HTTP_PATH.'administration/index.php?node_id='.$this->current_node()->get('node_id'));
		}

		if(isset($_GET['feed'])){//This is a special view that discard all other html and css
			$this->set_view('feed', true);
			include $this->get_view_path();
			exit;
		}

		if(isset($_GET['search'])){
			$this->set_view('search', true);
		}

		if(isset($_GET['login'])){
			if($this->config->user()->is_connected()){
			  $this->redirect($this->current_node()->link());
			}
			$this->set_view('login', true);
		}

		if(isset($_GET['config'])){
			$this->set_view('config', true);
		}

		if(isset($_GET['subscribe'])){
			if($this->config->user()->is_connected()){
				$this->config->error()->add(27);
			} else {
				$this->set_view('subscribe', true);
			}
		}

		if(isset($_GET['credits'])){
			$this->set_view('credits', true);
		}
	}

	/**
	 * This function focus specifically on node managment
	 * If you want to have a custom behavior for a type of node,
	 * just create or override the function load_[type_name]()
	 */
	protected function load_generic(){
		$this->set_view('read');
		if(isset($_GET['add']) && $this->allow_add){
			$this->add_generic();
		}
		if(isset($_GET['edit']) && $this->allow_edit){
			$this->edit_generic();
			if($_GET['edit'] == 'permissions'){
				$this->set_view('edit.permissions');
				if(isset($_POST['add_new_entity'])){
					$this->save_generic(array('edit'=>'permissions'), 'permissions');
				}
				if(isset($_GET['remove'])){
					$this->remove_permission($_GET['remove']);
				}
				if(isset($_POST['save'])){
					$this->save_generic(array('edit'=>'permissions'), 'permissions');
				}
				if(isset($_POST['submit'])){
					$this->save_generic('edit', 'permissions');
				}
			}
			if($_GET['edit'] == 'move'){
				$this->set_view('edit.move');
				if(isset($_POST['save'])){
					$this->save_generic(array('edit'=>'move'), 'location');
				}
				if(isset($_POST['submit'])){
					$this->save_generic('edit', 'location');
				}
			}
			if($_GET['edit'] == 'copy'){
				$this->set_view('edit.copy');
			}
			if(isset($_POST['save'])){
				$this->save_generic('edit');
			}
			if(isset($_POST['submit'])){
				$this->save_generic();
			}
		}
		if(isset($_GET['delete']) && $this->allow_delete){
			$this->set_view('delete');
			if(isset($_POST['delete'])){
				$this->delete_generic();
			}
		}
		if(isset($_GET['download']) && $this->allow_download){
			$this->download_original();
		}
		if(isset($_GET['thumb']) || isset($_GET['preview'])){//Loading applications for nodes
			$this->display_media(isset($_GET['preview']));
		}
	}

	protected function subscribe(){
		if(!isset($_POST['add_username'])){
			return false;
		}
		if($this->form->validate()){
			$id = node_user::add_node($this->config, $this->config->node(3), $this->form);
			if($id){
				$this->login($this->form->get_value('username'), $this->form->get_value('passwd'));
				$this->redirect($this->node($id)->link());
			}
		}
	}

	public function redirect($url){
		if($this->debug){
			echo '<a href="'.$url.'">Continue to '.$url.'</a>';
			exit;
		}
		$url = str_replace('&amp;', '&', $url);
		header('Location: '.$url);
		exit;
	}

	/**
	 * Default login action
	 */
	protected function login($username,$sha1){
		$this->config->user()->login($username,$sha1);
	}

	/**
	 * Default logout action
	 */
	protected function logout(){
		$this->config->user()->logout();
		$this->redirect($this->current_node()->link());
	}

	/**
	 * Default session removal action
	 */
	protected function remove_session($session_id){
		if($this->config->user()->remove_session($session_id)){
			$this->redirect($this->current_node()->link('config'));
		}
	}

	protected function remove_permission($id){
		if($this->current_node()->remove_permission($id)){
			$this->redirect($this->current_node()->link(array('edit'=>'permissions')));
		}
	}

	/**
	 * Default node suppression action : takes data from $_POST to delete the current node
	 * $_POST['delete'] : indicates that the user wants to delete the current node
	 */
	protected function delete_generic(){
		if(!$this->current_node()->get_auth('delete')){//If user doesn't have the edit permissions
			$this->config->error()->add(15);
			return false;
		}
		$parent=$this->current_node()->get_parent();
		if(!$this->current_node()->delete()){
			$this->redirect($this->current_node()->link());
		}
		$this->redirect($parent->link());
	}

	protected function edit_generic(){
		if(!$this->current_node()->get_auth('edit')){//If user doesn't have the edit permissions
			$this->config->error()->add(14);
			return false;
		}
		$this->set_view('edit',false,$this->current_node()->get('type_name'));
		$this->form=$this->current_node()->form();
		$this->form->set_action($this->current_node()->link('edit'));
		$this->form->set_title($this->config->localize('Edit element').' : '.$this->config->get_localized_type($this->current_node()->get('type_name')));
	}

	protected function save_generic($redirect=null, $form=null){
		if($this->current_node()->edit($form)){
			$this->config->error()->add(29, 0, 'Node '.$this->current_node()->get('node_id').' edited by '.$this->config->user());
			$this->redirect($this->current_node()->link($redirect));
		}
	}


	/**
	 * Load the default add view
	 * @return <type>
	 */
	protected function add_generic(){
		if(!$this->current_node()->get_auth('add')){//If user doesn't have the edit permissions
			$this->config->error()->add(13);
			return false;
		}
		$this->set_view('add',true);
		$type_name=$_GET['add'];
		if($this->config->is_type($type_name)){
			$this->set_view('add',false,$type_name);
			$class_name=$this->config->get_class_name($type_name);
			require_once $this->config->get_class_path($type_name);
			$this->form=call_user_func(array($this->config->get_class_name($type_name),'get_add_form'),$this->current_node()->link(array('add'=>$type_name)));
			$this->form->set_title($this->config->localize('New element').' : '.$this->config->get_localized_type($type_name));
			$this->form->add(new ui_input('type_name', '', 'hidden', $type_name));
			if(isset($_POST['submit'])){
				$this->add_real_generic($this->form);
			}
		}
	}

	/**
	 * Add a new node in the system
	 * @param <type_name> $type
	 * @param <string> $title
	 * @param <string> $content
	 * @param <string> $tags
	 * @return <boolean> false (if error)
	 */
	protected function add_real_generic($form){
		$type_name=$form->get_value('type_name');
		if(!$this->config->is_type($type_name)){
			return false;
		}
		$class_name=$this->config->get_class_name($type_name);
		require_once $this->config->get_class_path($type_name);
		$id=call_user_func(array($this->config->get_class_name($type_name),'add_node'),$this->config, $this->current_node(), $form);
		if($id == false){
			return false;
		}
		$this->redirect($this->config->node($id)->link());
	}

	/**
	 * Display the preview or the thumbnail
	 * @param <type> $preview
	 * @return <type>
	 */
	protected function display_media($preview=false){
		if(!$this->current_node()->get_auth('read')){//If user doesn't have the edit permissions
			$this->config->error()->add(12);
			return false;
		}
		if($preview){
			$file=$this->current_node()->get_preview();
		} else {
			$file=$this->current_node()->get_thumb();
		}
		if($file != false){
			$this->display_file($file);
		} else{
			$this->config->error()->add(28);
			return false;
		}
	}

	protected function download_original(){
		if(!$this->current_node()->get_auth('read')){//If user doesn't have the edit permissions
			$this->config->error()->add(12);
			return false;
		}
		$file=$this->current_node()->get_original();
		if($file != false){
			$this->download_file($file);
		} else{
			$this->config->error()->add(28);
			return false;
		}
	}
	/**
	 * Call this function to download a file.
	 * WARNING: You must secure the access to the file before calling this function.
	 * @param <type> $file
	 */
	protected function download_file($file){
		if(!isset($file['filename'],$file['size'],$file['path'])){
			//TODO: Throw error !
			return false;
		}
		header('Content-disposition: attachment; filename='.$file['filename']);
		header('Content-Type: application/force-download');
		header('Content-Transfer-Encoding: binary');
		header('Content-Length: '.$file['size']);
		header('Pragma: no-cache');
		header('Expires: 0');
		@ob_clean();
		flush();
		readfile($file['path']);
		exit;
	}

	/**
	 * Call this function to display an image.
	 * WARNING: You must secure the access to the image before calling this function.
	 * @param <type> $file
	 */
	protected function display_file(array $file){
		if(!isset($file['size'],$file['date'],$file['type'],$file['path'])){
			//TODO: Throw error !
			return false;
		}
		$etag='"'.md5($file['size'].$file['date']).'"';
		header('Etag: '.$etag);
		header('Expires: '.gmdate('D, d M Y H:i:s', time() + 31536000).' GMT');
		header('Cache-Control: public, max-age=31536000');
		header('Pragma: public');
		if(isset($_SERVER['HTTP_IF_NONE_MATCH'])){
			if($etag == $_SERVER['HTTP_IF_NONE_MATCH']){
				header("HTTP/1.0 304 Not Modified");
				header('Content-Length: 0');
				exit;
			}
		}
		header('Last-Modified: '.$file['date']);
		header('Content-Type: '.$file['type']);
		header('Content-Length: '.$file['size']);
		@ob_clean();
		flush();
		readfile($file['path']);
		exit;
	}

	/**
	 * Return the path to the active view
	 * @return <string> Path
	 */
	public final function get_view_path(){
		return $this->view_path;
	}

	/**
	 * Return the active form
	 * @return <ui_form> Form
	 */
	public final function get_form(){
		return $this->form;
	}

	/*
	 * Try to load a view and load generic view if it can't.
	 * Careful: The viewname variable is absolutely not secured, do not pass user-input !
	 */
	protected final function set_view($viewname,$absolute=false,$type=null){
		if($type==null){
			$type=$this->current_node()->get('type_name');
		}
		if($viewname == 'read' && $this->current_node()->get('override_view')){
			$viewname = $this->current_node()->get('override_view');
		}
		$this->view_path=PROJECT_REAL_PATH.'views/'.($absolute?'':$type.'/').$viewname.'.php';//Try the best-suited view in the current project
		if(!file_exists($this->view_path)){
			$this->view_path=REAL_PATH.'views/'.($absolute?'':$type.'/').$viewname.'.php';//Try the best-suited view in the framework
		}
		if(!file_exists($this->view_path)){
			$this->view_path=PROJECT_REAL_PATH.'views/'.($absolute?'':'generic/').$viewname.'.php';//Try the generic view in the project
		}
		if(!file_exists($this->view_path)){
			$this->view_path=REAL_PATH.'views/'.($absolute?'':'generic/').$viewname.'.php';//Try the generic view in the framework
		}
		if(!file_exists($this->view_path)){
			$this->view_path=PROJECT_REAL_PATH.'views/generic/read.php';//Try the most generic view in the project
		}
		if(!file_exists($this->view_path)){
			$this->view_path=REAL_PATH.'views/generic/read.php';//Fallback to the most generic view in the framework
		}
		$this->viewname=$viewname;
	}

	/**
	 * Return the current active node of the view
	 * @return <node>
	 */
	public final function current_node(){
		if($this->current_node_id !== null){
			return $this->config->node($this->current_node_id);
		}
		return $this->config->node(DEFAULT_NODE_ID);
	}

	/*
	 * This function is necessary to redirect alias to their pointed node.
	 * If you do not want to redirect automatically alias, override this function
	 */
	protected function load_alias(){
		$this->load_generic();
		if($this->current_node()->get_pointed_node() != $this->current_node() && $this->viewname=='read'){
			$this->redirect($this->current_node()->get_pointed_node()->link());
		}
	}

}

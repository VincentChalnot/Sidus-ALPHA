<?php

/**
 * Database abstraction class
 */
class sys_database{

	private $db;
	private $debug=true; //Display and throw error on each error
	private $profile=false; //Record every query

	public function __construct($db_link){ //Create the handler with the database
		$this->db=new SQLite3($db_link);
		if($this->db == false){
			trigger_error('Can\'t connect to the database');
			exit;
		}
	}

	public static function test($db_link){ //Test the database
		try{
			$db=new SQLite3($db_link);
		}catch(Exception $e){
			return false;
		}
		return true;
	}

	public function exec($query){ //Execute a query
		$this->profile('exec', $query);
		if($this->db->exec($query) !== false){
			return true;
		}
		$this->debug('exec', $query);
		return false;
	}

	public function get_single($query){ //Return the first value of the query
		$this->profile('get_single', $query);
		$result=$this->db->querySingle($query);
		if($result !== false){
			return $result;
		}
		$this->debug('get_single', $query);
		return false;
	}

	public function get_row($query){ //Return a row
		$this->profile('get_row', $query);
		$result=$this->db->querySingle($query, true);
		if($result !== false){
			return $result;
		}
		$this->debug('get_row', $query);
		return false;
	}

	public function get_array($query){ //Return an array for multiple rows
		$this->profile('get_array', $query);
		$result=$this->db->query($query);
		if($result !== false){
			$array=array();
			while($row=$result->fetchArray(SQLITE3_ASSOC)){
				$array[]=$row;
			}
			return $array;
		}
		$this->debug('get_array', $query);
		return false;
	}

	public function get_last_id(){ //Get the last id inserted
		return $this->db->lastInsertRowID();
	}

	public function get_last_error(){ //Get the last error
		return $this->db->lastErrorMsg();
	}

	public function secure_input($string){//Secure a string for: SQL query (injection-safe), remove all HTML (XSS-safe)
		if($string !== null){
			return $this->db->escapeString($string);
		}else{
			return null;
		}
	}

	public function get_type(){
		return 'sqlite3';
	}

	function __destruct(){
		return $this->db->close();
	}

	private function debug($label, $query){
		if($this->debug){
			trigger_error('<div style="position:absolute;background:red;width:900px;padding:10px;"><b>MySQL '.$label.' : </b>'.$query.'<br><b>Message : </b>'.$this->get_last_error().'</div>');
		}
	}

	private function profile($label, $query){
		if($this->profile){
			file_put_contents('profile.sql', '--exec at '.date('c').':'."\n".$query."\n", FILE_APPEND);
		}
	}
	
	public function begin_transaction(){
		$this->exec('BEGIN TRANSACTION');
	}
	
	public function commit_transaction(){
		$this->exec('COMMIT TRANSACTION');
	}
	
	public function rollback_transaction(){
		$this->exec('ROLLBACK');
	}

}

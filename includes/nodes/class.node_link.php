<?php

/**
 * Allow to manage images, videos, sounds and documents
 *
 */
class node_link extends node_generic{

	protected function get_more(){
		$query='SELECT media_url FROM node_media WHERE node_id='.$this->infos['node_id'];
		$this->infos=array_merge($this->infos, (array)$this->config->db()->get_row($query));
	}

	protected static function add_more($id, sys_config $config, node_generic $parent_node, $form){
		$query='INSERT INTO node_media (node_id, media_url) VALUES ('.$id.', \''.$config->secure_input($form->get_value('media_url')).'\')';
		if(!$config->db()->exec($query)){
			$this->config->error()->add(0);//TODO Create error
			return false;
		}
		return true;
	}

	protected function delete_more(){
		$query='DELETE FROM node_media WHERE node_id='.$this->infos['node_id'];
		if(!$this->config->db()->exec($query)){
			$this->config->error()->add(0);//TODO Create error
			return false;
		}
		return true;
	}

	protected function edit_more($form){
		//Editing node_info
		$list=array();
		$list[]='media_url=\''.$this->config->secure_input($this->form()->get_value('media_url')).'\'';
		$query='UPDATE node_media SET '.implode(', ', $list).' WHERE node_id='.$this->infos['node_id'];
		if(!$this->config->db()->exec($query)){
			$this->config->error()->add(0);//TODO Create error
			return false;
		}
		return true;
	}

	public static function get_add_form($action='', $method='post'){
		$form = parent::get_add_form($action, $method);
		$form->add(new ui_input('media_url', 'URL : ', 'text'), 2);
		return $form;
	}

	protected function init_more_form(){
		$this->content_form->add(new ui_input('media_url', 'URL : ', 'text', $this->get('media_url', false)), 1);
	}

	public function get_html_content(){
		if(!$this->auths['read']){//Test permissions
			$this->add_read_error();
			return false;
		}
		$string='<p><a href="'.$this->get('media_url').'">'.$this.' ('.$this->get('media_url').')</a></p>';
		$string.=$this->get('content');
		return $string;
	}

}

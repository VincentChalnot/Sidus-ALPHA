<?php

/**
 * Database abstraction class
 */
class sys_database
{

	private $db;
	private $debug = true; //Display and throw error on each error
	private $profile = false; //Record every query

	public function __construct($host, $user, $passwd, $database, $port)
	{ //Create the handler with the database
		$this->db = mysqli_init();
		if (!$this->db) {
			trigger_error('MySQLi initialization failed');
			exit;
		}
		$this->db->options(MYSQLI_INIT_COMMAND, 'SET NAMES utf8');
		if ($this->db->real_connect($host, $user, $passwd, null, $port)) {
			if (!$this->db->select_db($database)) {
				trigger_error('Database '.$database.' doesn\'t exists.');
				exit;
			}
		} else {
			trigger_error('Can\'t connect to the database');
			exit;
		}
	}

	public static function test($host, $user, $passwd, $database)
	{ //Test the database
		$db = mysqli_init();
		if (!$db) {
			return 'MySQLi initialization failed';
		}
		if (!$db->real_connect($host, $user, $passwd)) {
			return 'Can\'t connect to the database';
		}
		if (!$db->select_db($database)) {
			return 'Database '.$database.' doesn\'t exists.';
		}
		return true;
	}

	public function exec($query)
	{ //Execute a query
		$this->profile('exec', $query);
		if ($this->db->real_query($query)) {
			return true;
		}
		$this->debug('exec', $query);
		return false;
	}

	public function get_single($query)
	{ //Return the first value of the query
		$this->profile('get_single', $query);
		if ($this->db->real_query($query)) {
			$result = $this->db->use_result();
			if ($result) {
				$tmp = $result->fetch_row();
				$result->close();
				return $tmp[0];
			}
		}
		$this->debug('get_single', $query);
		return false;
	}

	public function get_row($query)
	{ //Return a row
		$this->profile('get_row', $query);
		if ($this->db->real_query($query)) {
			$result = $this->db->use_result();
			if ($result) {
				$tmp = $result->fetch_assoc();
				$result->close();
				return $tmp;
			}
		}
		$this->debug('get_row', $query);
		return false;
	}

	public function get_array($query)
	{ //Return an array for multiple rows
		$this->profile('get_array', $query);
		if ($this->db->real_query($query)) {
			$result = $this->db->use_result();
			if ($result) {
				$array_results = array();
				while ($tmp = $result->fetch_assoc()) {
					$array_results[] = $tmp;
				}
				$result->close();
				return $array_results;
			}
		}
		$this->debug('get_array', $query);
		return false;
	}

	public function get_last_id()
	{ //Get the last id inserted
		return $this->db->insert_id;
	}

	public function get_last_error()
	{ //Get the last error
		return $this->db->error;
	}

	public function secure_input($string)
	{//Secure a string for: SQL query (injection-safe)
		if ($string !== null) {
			return $this->db->real_escape_string($string);
		} else {
			return null;
		}
	}

	public function get_type()
	{
		return 'mysql';
	}

	public function __destruct()
	{
		return $this->db->close();
	}

	private function debug($label, $query)
	{
		if ($this->debug) {
			trigger_error('<div style="position:absolute;background:red;width:900px;padding:10px;"><b>MySQL '.$label.' : </b>'.$query.'<br><b>Message : </b>'.$this->get_last_error().'</div>');
		}
	}

	private function profile($label, $query)
	{
		if ($this->profile) {
			file_put_contents('profile.sql', '--'.$label.' at '.date('c').':'."\n".$query."\n", FILE_APPEND);
		}
	}

	public function begin_transaction()
	{
		$this->exec('START TRANSACTION');
	}

	public function commit_transaction()
	{
		$this->exec('COMMIT');
	}

	public function rollback_transaction()
	{
		$this->exec('ROLLBACK');
	}

}

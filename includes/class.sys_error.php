<?php

/**
 * Error handling class, uses /framework/localization/errors.[lang].ini for localization
 * Warning: If $this->config->session is null, it means the session hasn't been initialized yet,
 * so you can't send error messages to the user. (Yet there are stored in the database)
 * @author Vincent Chalnot
 */
class sys_error{

	private $config; //class.sys_config.php
	private $messages=array();
	private $current=array();
	private $lang;

	public function __construct(sys_config $config){
		$this->config=$config; //Get handler from the current Database
		$tmp_path=REAL_PATH.'includes/localization/errors.'.$this->config->get('lang').'.ini'; //Path to language file
		if(!file_exists($tmp_path)){//Check the existence of the file
			trigger_error('ABoard : No localization file at '.$tmp_path, E_USER_ERROR);
			exit;
		}
		$this->messages=parse_ini_file($tmp_path, 'database'); //Parsing the localization file
		$this->messages=$this->messages['errors']; //Getting all errors
		set_exception_handler(array($this, 'exception_handler'));
	}

	public function exception_handler($e){
		echo '<div style="display:block;position:absolute;background:rgba(255,255,255,0.8);width:500px;"><h2>ERROR!</h2><pre>';
		var_dump($e);
		echo '</pre></div>';
	}

	/**
	 * TODO : rewrite the all damn thing
	 */
	public function get_all(){
		if(!$this->config->user()->is_connected()){
			return $this->current;
			//Not sure if we need to crash here
			trigger_error('ABoard : Session not initialized. Can\'t get errors with no session opened.', E_USER_ERROR);
			exit;
		}
		//PARSE ALL ERRORS FROM DB
		$query='SELECT * FROM sys_error WHERE user_id='.$this->config->user()->get('node_id').' AND flag=0 ORDER BY error_date';
		$tmp=$this->config->db()->get_array($query);
		if($tmp != false){//Parse errors from DB
			$errors=array();
			foreach($tmp as $value){
				if(isset($this->messages[$value['error_code']])){
					$value['user_message']=$this->messages[$value['error_code']];
				}else{
					$value['user_message']='Unknown error '.$value['error_code'];
				}
				$errors[]=$value;
			}
			return $errors;
		}
		//If a problem occured during query
		return array();
	}

	public function add($code, $severity=1, $message=''){
		if($severity == 2 && $this->config->debug){
			echo '<div style="display:block;position:absolute;background:rgba(255,255,255,0.8);width:500px;">';
			xdebug_print_function_stack('An error occured : '.$message);
			echo '</div>';
		}
		$error=array();
		$backtrace=debug_backtrace();
		$error['code']=(int)$code;
		$error['date']=$this->config->date()->now();
		$error['severity']=(int)$severity;
		$error['message']=$message;
		$error['user_message']=$this->messages[$error['code']];
		$error['file']=$backtrace[0]['file'];
		$error['line']=(int)$backtrace[0]['line'];
		$error['method']=$backtrace[0]['function'];
		$error['user_id']='NULL';
		if($this->config->user() != false){
			if($this->config->user()->is_connected()){
				$error['user_id']=(int)$this->config->user()->get('node_id');
			}
		}
		$error['remote_addr']=$_SERVER['REMOTE_ADDR']; //Get remote address
		$error['user_agent']=$_SERVER['HTTP_USER_AGENT']; //Get user-agent
		$query='INSERT INTO sys_error
			(error_code, error_date, error_severity, error_message, error_file, error_line, error_method,	user_id, user_agent, remote_addr) VALUES
			('.$error['code'].','.$error['date'].','.$error['severity'].',\''.$this->config->secure_input($error['message']).'\',\''.$this->config->secure_input($error['file']).'\','.$error['line'].',\''.$this->config->secure_input($error['method']).'\','.$error['user_id'].',\''.$this->config->secure_input($error['user_agent']).'\',\''.$this->config->secure_input($error['remote_addr']).'\')';
		$this->current[]=$error;
		return $this->config->db()->exec($query);
	}

	public function remove($id){
		$id=(int)$id;
		if(!$this->config->user()->is_connected()){
			return false;
			//Not sure if we need to crash there
			trigger_error('ABoard : Session not initialized. Can\'t remove error with no session opened.', E_USER_ERROR);
			exit;
		}
		$query='UPDATE sys_error SET flag=1 WHERE user_id='.$this->config->user()->get('node_id').' AND error_id='.$id;
		return $this->config->db()->exec($query);
	}

	public function clear(){
		if(!$this->config->user()->is_connected()){
			return false;
			//Not sure if we need to crash there
			trigger_error('ABoard : Session not initialized. Can\'t clear errors with no session opened.', E_USER_ERROR);
			exit;
		}
		$query='UPDATE sys_error SET flag=1 WHERE user_id='.$this->config->user()->get('node_id');
		return $this->config->db()->exec($query);
	}

}

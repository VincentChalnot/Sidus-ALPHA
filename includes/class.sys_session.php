<?php

/**
 * Session class.
 * This object is used to identify a user (connected or not)
 * It uses a cookie (like PHP sessions) to pass the session signature along the pages
 * TODO: Eventually we need to make it more compatible with a GET signature...
 * @author Vincent Chalnot
 */
define('COOKIE_NAME', 'signature');

class sys_session{

	private $config;
	private $session=array();

	public function __construct(sys_config $config){
		$this->config=$config; //Get handler from the current Database
		$this->init_session();
	}

	private function init_session(){
		if(!isset($_COOKIE[COOKIE_NAME])){
			return false;
		}
		$signature=$_COOKIE[COOKIE_NAME];
		$query='SELECT * FROM sys_session WHERE signature=\''.$this->config->secure_input($signature).'\' AND expire >= '.$this->config->date()->now(); //Get active sessions from db
		$this->session=$this->config->db()->get_row($query);
		if($this->session == false){//If there is no session in database
			$this->config->error()->add(1); //Session expired
			setcookie(COOKIE_NAME, '', time() - 3600, '/'); //Destroy session cookie
			return false;
		}
		//Passed this point, session has been initialized, but we also check if the IP and user agent are the same (preventing session hijack)
		$remote_addr=$_SERVER['REMOTE_ADDR']; //Get remote address
		$user_agent=$_SERVER['HTTP_USER_AGENT']; //Get User-Agent
		if($remote_addr != $this->session['remote_addr'] || $user_agent != $this->session['user_agent']){//Check if IP, User Agent are correct
			$this->session=null;
			$this->config->error()->add(1); //Session expired
			setcookie(COOKIE_NAME, '', time() - 3600, '/'); //Destroy session cookie
			return false;
		}
	}

	public function create_session($user_id){
		if(!$this->config->user()->is_connected()){
			$this->config->error()->add(21, 2, 'Trying to create session while not authenticated');
			return false;
		}
		$signature=sha1(rand(0, 10000000000)).sha1(rand(0, 10000000000).time()); //Create a random and unique signature
		$remote_addr=$_SERVER['REMOTE_ADDR']; //Get remote address
		$user_agent=$_SERVER['HTTP_USER_AGENT']; //Get User-Agent
		$expire=time() + $this->config->get('session_expire'); //Define expiration time for both cookie and database
		setcookie(COOKIE_NAME, $signature, $expire, '/'); //Set session cookie
		$query='INSERT INTO sys_session(user_id,signature,remote_addr,user_agent,expire) VALUES ('.$user_id.',\''.$signature.'\',\''.$this->config->secure_input($remote_addr).'\',\''.$this->config->secure_input($user_agent).'\','.$this->config->date()->from_timestamp($expire).')';
		$this->config->db()->exec($query); //Store session infos in database
		header('Location: '.$_SERVER['REQUEST_URI']); //Reload
		$query='DELETE FROM sys_session WHERE expire < '.$this->config->date()->now(); //Delete all expired sessions
		$this->config->db()->exec($query);
		exit;
	}

	/**
	 * Get a session variable
	 * @param <String> $key
	 * @return <String>
	 */
	public function get($key){
		if(array_key_exists($key, $this->session)){
			return $this->session[$key];
		}else{
			$this->config->error()->add(20, 1, 'Non existing key for session (user probably not connected) : '.$key);
			return false;
		}
	}

	public function exists(){
		if($this->session == false){
			return false;
		}
		return true;
	}

}

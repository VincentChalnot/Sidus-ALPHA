<?php if(!isset($nodes, $board)){trigger_error('You must set $nodes and $board before loading the following component :'.$_SERVER['SCRIPT_NAME']);return;} ?>
<div class="preview_view">
	<?php if($nodes == false): ?>
		<div class="empty"><?php echo $board->config->localize('Empty') ?></div>
	<?php else: ?>
		<?php foreach($nodes as $child): ?>
			<?php if($child->get_auth('read')): ?>
				<div class="element">
					<div class="left">
						<a href="<?php echo $child->link() ?>"><?php echo $child->get_html_thumb(ICON_BIG) ?></a>
					</div>
					<div class="right">
						<?php if($child->get_auth('add')): ?>
							<a href="<?php echo $child->link('add') ?>"><?php echo $board->generate_icon('add') ?></a>
						<?php endif ?>
						<?php if($child->get_auth('edit')): ?>
							<a href="<?php echo $child->link('edit') ?>"><?php echo $board->generate_icon('edit') ?></a>
						<?php endif ?>
						<?php if($child->get_auth('delete')): ?>
							<a href="<?php echo $child->link('delete') ?>"><?php echo $board->generate_icon('delete') ?></a>
						<?php endif ?>
					</div>
					<div class="head">
						<a href="<?php echo $child->link() ?>"><?php echo $child ?></a> <?php echo $board->config->localize('by') ?>
						<?php if($child->get_owner() == false): ?>
							<a><?php echo $board->config->localize('Nobody') ?></a>
						<?php else: ?>
							<a href="<?php echo $child->get_owner()->link() ?>"><?php echo $child->get_owner() ?></a>
						<?php endif ?>
						<?php echo $board->config->localize('the') ?>
						<span title="<?php echo $child->get_creation() ?>"><?php echo $child->get_creation(DATE_TINY) ?></span>
					</div>
					<article>
						<?php if ($text = $board->convert_to_text($child->get('content'), 300)) : ?>
							<p>
								<?php echo $text ?>
								<a href="<?php echo $child->link() ?>">Lire la suite</a>
							</p>
						<?php endif ?>
					</article>
				</div>
			<?php endif ?>
		<?php endforeach ?>
	<?php endif; ?>
</div>
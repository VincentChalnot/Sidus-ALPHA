<?php

/**
 * Allow to manage images, videos, sounds and documents
 *
 */
class node_media extends node_generic {

	/**
	 * Initializing basic node
	 */
	public function __construct(sys_config $config, $id) {
		parent::__construct($config, $id);
	}

	protected function get_more() {
		$query = 'SELECT * FROM node_media WHERE node_id='.$this->infos['node_id'];
		$this->infos = array_merge($this->infos, (array) $this->config->db()->get_row($query));
		$this->infos['original_path'] = REAL_PATH.$this->config->get('originals_directory').$this->infos['node_id'];
	}

	protected static function add_more($id, sys_config $config, node_generic $parent_node, $form) {
		$tmp = parent::add_more($id, $config, $parent_node, $form);
		if ($tmp == false) {
			return false;
		}
		$query = 'INSERT INTO node_media (node_id) VALUES ('.$id.')';
		if (!$config->db()->exec($query)) {
			//TODO: throw an error
			return false;
		}
		return true;
	}

	public function post_add(ui_form $form) {
		$this->edit_more($form);
	}

	protected function delete_more() {
		$this->delete_original();
		$this->delete_preview();
		$this->delete_thumb();

		$query = 'DELETE FROM node_media WHERE node_id='.$this->infos['node_id'];
		if (!$this->config->db()->exec($query)) {
			//TODO: Throw some error
			return false;
		}
		return true;
	}

	protected function delete_original() {
		$file = $this->get_original();
		if (!file_exists($file['path'])) {
			return false;
		}
		unlink($file['path']);
		return true;
	}

	protected function delete_preview() {
		$file = $this->get_preview();
		if (!file_exists($file['path'])) {
			return false;
		}
		unlink($file['path']);
		return true;
	}

	protected function delete_thumb() {
		$file = $this->get_thumb(true);
		if ($file === null) {
			return false;
		}
		if (!file_exists($file['path'])) {
			return false;
		}
		unlink($file['path']);
		return true;
	}

	public static function get_add_form($action = '', $method = 'post') {
		$form = parent::get_add_form($action, $method);
		require_once REAL_PATH.'includes/interface/class.ui_file.php';
		require_once REAL_PATH.'includes/interface/class.ui_hidden.php';
		$tmp_file = REAL_PATH.TMP_DIRECTORY.time().'.tmp';
		$form->add(new ui_file('file', 'File :', $tmp_file), 2); //Don't forget to add more file type when available
		$form->add(new ui_input('media_url', 'URL : ', 'text'), 3);
		$form->add(new ui_hidden('new_file', 1));
		return $form;
	}

	protected function edit_more($form) {
		//Editing node_info
		$list = array();
		$file = $form->get_value('file');
		$tmp_name = isset($file['tmp_name']) ? $file['tmp_name'] : '';
		$new_name = REAL_PATH.$this->config->get('originals_directory').$this->infos['node_id'];
		if ($form->get_value('new_file') && $tmp_name) {
			if (!@rename($tmp_name, $new_name)) {
				trigger_error('Unable to move uploaded file', E_USER_WARNING);
			}
		}
		if ($tmp_name) {
			$list[] = 'filename=\''.$this->config->secure_input($file['name']).'\''; //AUTODETECT
			switch ($this->get('type_name')) {
				case 'document':
					$type = $this->config->secure_input($file['type']);
					$list[] = 'media_type=\''.$this->config->secure_input((string) $type).'\'';
					break;
				case 'image':
					$type = $this->create_thumbnail(true); //Also generate preview
					$list[] = 'media_type=\''.$this->config->secure_input((string) $type).'\'';
					break;
				case 'icon':
					$type = $this->create_thumbnail();
					$list[] = 'media_type=\''.$this->config->secure_input((string) $type).'\'';
					break;
			}
		} elseif ($form->get_value('delete')) {
			$this->delete_original();
			$this->delete_preview();
			$this->delete_thumb();
		}
		if ($form->get_value('media_url') !== false) {
			$list[] = 'media_url=\''.$this->config->secure_input($form->get_value('media_url')).'\'';
		}
		if (count($list)) {
			$query = 'UPDATE node_media SET '.implode(', ', $list).' WHERE node_id='.$this->infos['node_id'];
			if (!$this->config->db()->exec($query)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Getting the thumbnail
	 */
	public function get_thumb($force_uploaded_file = false) {
		if (!$this->get_auth('read')) {
			//If not authorized return default icon
			return $this->config->get_thumb($this->get('type_name', false));
		}
		$path = REAL_PATH.$this->config->get('thumbs_directory').$this->infos['node_id'];
		if (!file_exists($path)) {
			if ($force_uploaded_file) {
				return null;
			}
			return parent::get_thumb();
		}
		list($width, $height) = getimagesize($path);
		$file = array(
			'filename' => $this->get('filename'),
			'url' => $this->link(array('thumb' => $this->get('filename'))),
			'path' => $path,
			'width' => $width,
			'height' => $height,
			'size' => filesize($path),
			'date' => filemtime($path),
			'type' => $this->get('media_type')
		);
		return $file;
	}

	public function get_preview() {
		$path = REAL_PATH.$this->config->get('previews_directory').$this->infos['node_id'];
		if (!$this->get_auth('read')) {
			return false;
		}
		if($this->get('type_name') == 'video'){
			return $this->get_video_preview();
		}
		if (!file_exists($path)) {
			return false;
		}
		list($width, $height) = getimagesize($path);
		$file = array(
			'filename' => $this->get('filename'),
			'url' => $this->link(array('preview' => $this->get('filename'))),
			'path' => $path,
			'width' => $width,
			'height' => $height,
			'size' => filesize($path),
			'date' => filemtime($path),
			'type' => $this->get('media_type')
		);
		return $file;
	}

	public function get_html_preview($with_title = true, $max_width = false, $max_height = false, $cut_height = false) {
		if (!$this->auths['read']) {//Test permissions
			$this->add_read_error();
			return false;
		}
		$max_image_width = $max_width ? $max_width : (int) $this->config->get('max_image_width');
		$max_image_height = $max_height ? $max_height : (int) $this->config->get('max_image_height');
		$width = 0;
		$height = 0;
		$str = '';

		$file = $this->get_preview();
		if ($file != false) {
			$url = $file['url'];
			$width = $file['width'];
			$height = $file['height'];
		} elseif ($this->get('media_url') != '') {
			$url = $this->get('media_url');
			list($width, $height) = getimagesize($this->get('media_url'));
		}

		if ($width > 0 && $height > 0) {
			if ($width > $max_image_width) {//If too large
				$height = round($height * $max_image_width / $width);
				$width = $max_image_width;
			}
			if ($height > $max_image_height) {//If too high
				$width = round($width * $max_image_height / $height);
				$height = $max_image_height;
			}
			$str.='<div class="image_preview"';
			if($cut_height){
				$str.=' style="height:'.$cut_height.'px;"';
			}
			$str.='>';
			$str.='<img src="'.$url.'" width="'.$width.'" />';
			if ($with_title) {
				$str.='<p>'.$this.'</p>';
			}
			$str.='</div>';
		}
		return $str;
	}

	public function get_original() {
		$path = REAL_PATH.$this->config->get('originals_directory').$this->infos['node_id'];
		if (!$this->get_auth('read')) {
			return false;
		}
		if (!file_exists($path)) {
			return false;
		}
		$file = array(
			'filename' => $this->get('filename'),
			'url' => $this->link(array('original' => $this->get('filename'))),
			'path' => $path,
			'size' => filesize($path),
			'date' => filemtime($path),
			'type' => $this->get('media_type')
		);
		return $file;
	}

	/**
	 * Check image, resize and thumbnailize
	 *
	 */
	public function create_thumbnail($preview = false) {
		$max_image_width = (int) $this->config->get('max_image_width');
		$max_image_height = (int) $this->config->get('max_image_height');
		$max_thumb_width = (int) $this->config->get('max_thumb_width');
		$max_thumb_height = (int) $this->config->get('max_thumb_height');

		$path = REAL_PATH.$this->config->get('originals_directory').$this->infos['node_id'];

		if (!file_exists($path)) {//If the file doesn't exists
			$this->config->error()->add(32);
			return false;
		}

		if (!function_exists('imageCreateFromJPEG')) {
			$this->config->error()->add(34);
			return false;
		}

		list($width, $height) = getimagesize($path);
		if ($width == 0 || $height == 0) {
			$this->config->error()->add(32);
			return false;
		}

		//Attempt to create an image from every format possible
		$image = @imageCreateFromJPEG($path);
		$type = 'image/jpeg';
		if ($image == false) {
			$image = @imageCreateFromGIF($path);
			$type = 'image/gif';
			if ($image == false) {
				$image = @imageCreateFromPNG($path);
				$type = 'image/png';
				if ($image == false) {
					$this->config->error()->add(32);
					return false;
				}
			}
		}

		if ($width > $height) {//Landscape
			$preview_width = $width;
			$preview_height = $height;
		} else { //Portrait
			//switching width and height
			$preview_width = $height;
			$preview_height = $width;
		}

		if ($preview_width > $max_image_width) {//If too large
			$preview_height = round($preview_height * $max_image_width / $preview_width);
			$preview_width = $max_image_width;
		}

		if ($preview_height > $max_image_height) {//If too high
			$preview_width = round($preview_width * $max_image_height / $preview_height);
			$preview_height = $max_image_height;
		}

		$thumb_height = $max_thumb_height;
		$thumb_width = round($preview_width * $max_thumb_width / $preview_height); //Setting the thumb size proportional to

		if ($width < $height) {//Portrait
			//switching width and height again
			$tmp = $preview_width;
			$preview_width = $preview_height;
			$preview_height = $tmp;
			$tmp = $thumb_width;
			$thumb_width = $thumb_height;
			$thumb_height = $tmp;
		}

		if ($preview) {//If the preview generation is enabled (default: false)
			//generating preview
			$final_image = imagecreatetruecolor((int) $preview_width, (int) $preview_height);
			imagealphablending($final_image, true);
			$transparent = imagecolorallocatealpha($final_image, 0, 0, 0, 127);
			imagefill($final_image, 0, 0, $transparent);
			if (!imagecopyresampled($final_image, $image, 0, 0, 0, 0, (int) $preview_width, (int) $preview_height, (int) $width, (int) $height)) {
				$this->config->error()->add(33);
				return false;
			}
			$preview_path = REAL_PATH.$this->config->get('previews_directory');
			if (!file_exists($preview_path)) {
				mkdir($preview_path);
			}
			$preview_path.=$this->infos['node_id'];
			switch ($type) {
				case 'image/jpeg':
					imageJPEG($final_image, $preview_path);
					break;
				case 'image/gif':
					imagesavealpha($final_image, true);
					imageGIF($final_image, $preview_path);
					break;
				case 'image/png':
					imagealphablending($final_image, false);
					imagesavealpha($final_image, true);
					imagePNG($final_image, $preview_path);
					break;
			}
		}

		//Generating thumbnail
		$thumb = imagecreatetruecolor((int) $thumb_width, (int) $thumb_height);
		imagealphablending($thumb, true);
		$transparent = imagecolorallocatealpha($thumb, 0, 0, 0, 127);
		imagefill($thumb, 0, 0, $transparent);
		if (!imagecopyresampled($thumb, $image, 0, 0, 0, 0, (int) $thumb_width, (int) $thumb_height, (int) $width, (int) $height)) {
			$this->config->error()->add(32);
			return false;
		}
		$thumb_final = imagecreatetruecolor($max_thumb_width, $max_thumb_height);
		imagealphablending($thumb_final, true);
		$transparent = imagecolorallocatealpha($thumb_final, 0, 0, 0, 127);
		imagefill($thumb_final, 0, 0, $transparent);
		if ($thumb_width == $max_thumb_width) {
			$x_thumb = 0;
			$y_thumb = round(($thumb_height - $max_thumb_height) / 2);
		} else {
			$x_thumb = round(($thumb_width - $max_thumb_width) / 2);
			$y_thumb = 0;
		}
		imagecopy($thumb_final, $thumb, 0, 0, (int) $x_thumb, (int) $y_thumb, $max_thumb_width, $max_thumb_height);
		$thumb_path = REAL_PATH.$this->config->get('thumbs_directory');
		if (!file_exists($thumb_path)) {
			mkdir($thumb_path);
		}
		$thumb_path.=$this->infos['node_id'];
		switch ($type) {
			case 'image/jpeg':
				imageJPEG($thumb_final, $thumb_path);
				break;
			case 'image/gif':
				imagesavealpha($thumb_final, true);
				imageGIF($thumb_final, $thumb_path);
				break;
			case 'image/png':
				imagealphablending($thumb_final, false);
				imagesavealpha($thumb_final, true);
				imagePNG($thumb_final, $thumb_path);
				break;
		}

		if (!(int) $this->config->get('keep_original')) {
			unlink(REAL_PATH.$this->config->get('originals_directory').$this->infos['node_id']);
		}
		return $type;
	}

	protected function init_more_form() {
		require_once REAL_PATH.'includes/interface/class.ui_file.php';
		require_once REAL_PATH.'includes/interface/class.ui_checkbox.php';
		if (!file_exists(REAL_PATH.$this->config->get('originals_directory'))) {
			mkdir(REAL_PATH.$this->config->get('originals_directory'));
		}
		switch ($this->get('type_name')) {
			case 'document':
				$allowed_types = null;
				break;
			case 'image':
			case 'icon':
				$allowed_types = array('jpeg', 'jpg', 'gif', 'png');
				break;
			default:
				$allowed_types = false;
		}
		if ($allowed_types !== false) {
			$this->content_form->add(new ui_file('file', 'File :', REAL_PATH.$this->config->get('originals_directory').$this->infos['node_id'], $allowed_types), 1); //Don't forget to add more file type when available
			$this->content_form->add(new ui_checkbox('delete', 'Delete current : '), 2);
		}
		$this->content_form->add(new ui_input('media_url', 'URL : ', 'text', $this->get('media_url', false)), 2);
	}

	public function get_html_content() {
		if (!$this->auths['read']) {//Test permissions
			$this->add_read_error();
			return false;
		}
		if($this->get('type_name') == 'video'){
			$string = $this->get_html_video();
		} else {
			$string = $this->get_html_preview();
		}
		$original = $this->get_original();
		if ($original && $this->get('type_name') == 'document') {
			$string.='<p>'.$this->config->localize('Download').' : <a href="'.$original['url'].'">'.$original['filename'].'</a> <i>('.proto_board::display_size($original['size']).')</i></p>';
		}
		$string.=$this->get('content');
		return $string;
	}

	public function get_html_video($width = null, $height = null, $class = null) {
		if (!$this->auths['read']) {//Test permissions
			$this->add_read_error();
			return false;
		}
		if ($width == null) {
			$width = $this->config->get('max_image_width');
		}
		if ($height == null) {
			$height = $this->config->get('max_image_height');
		}
		$url = parse_url($this->get('media_url'));
		if (!isset($url['host'], $url['path'])) {
			return false;
		}
		$final_url = null;
		switch ($url['host']) {
			case 'www.dailymotion.com':
				$path = pathinfo($url['path']);
				$final_url = 'http://www.dailymotion.com/embed/video/'.trim($path['basename'], '/');
				break;
			case 'www.youtube.com':
				$infos = array();
				parse_str(html_entity_decode($url['query']), $infos);
				if (isset($infos['v'])) {
					$final_url = 'http://www.youtube.com/embed/'.$infos['v'];
				}
				break;
			case 'vimeo.com':
				$final_url = 'http://player.vimeo.com/video/'.trim($url['path'], '/');
				break;
			default:
				$final_url = $this->get('media_url');
				break;
		}
		if ($final_url) {
			$str = '<iframe src="'.$final_url.'" frameborder="0" width="'.$width.'" height="'.$height.'" class="video_preview '.$class.'" webkitAllowFullScreen allowFullScreen></iframe>';
		} else {
			$str = '<p style="color:red">Sorry, unable to display the video, try a standard URL for Vimeo, Youtube or Dailymotion instead.</p>';
		}
		return $str;
	}

	public function get_video_preview(){
		if (!$this->auths['read']) {//Test permissions
			$this->add_read_error();
			return false;
		}
		$url = parse_url($this->get('media_url'));
		if (!isset($url['host'], $url['path'])) {
			return false;
		}
		switch ($url['host']) {
			case 'www.dailymotion.com':
				$path = pathinfo($url['path']);
				$final_url = 'http://www.dailymotion.com/thumbnail/video/'.trim($path['basename'], '/');
				break;
			case 'www.youtube.com':
				$infos = array();
				parse_str(html_entity_decode($url['query']), $infos);
				if (isset($infos['v'])) {
					$final_url = 'http://img.youtube.com/vi/'.$infos['v'].'/hqdefault.jpg';
				}
				break;
			case 'vimeo.com':
				$infos = json_decode(file_get_contents('http://vimeo.com/api/v2/video/'.trim($url['path'], '/').'.json'), true);
				$infos = array_pop($infos);
				$final_url = isset($infos['thumbnail_large']) ? $infos['thumbnail_large'] : '';
				break;
			default:
				$final_url = '';
				break;
		}
		if(!$final_url){
			return false;
		}
		list($width, $height) = getimagesize($final_url);
		$file = array(
			'filename' => $this->get('filename'),
			'url' => $final_url,
			'path' => $this->get('media_url'),
			'width' => $width,
			'height' => $height,
			'size' => 0,
			'date' => $this->get('modification'),
			'type' => $this->get('media_type')
		);
		return $file;
	}


	/* //Not working well
	  public function get_html_thumb($size=-1){
	  if($this->infos['type_name']!='video'){
	  return parent::get_html_thumb($size);
	  }
	  if($size < 0){
	  $size=$this->config->get('default_icon_size');
	  }
	  return '<span class="thumb">'.$this->get_html_video($size, $size).'</span>';
	  }
	 */
}

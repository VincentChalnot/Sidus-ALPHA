CREATE TABLE node_type (
	type_name VARCHAR(20) CHARACTER SET ascii NOT NULL,
	class_name VARCHAR(30) CHARACTER SET ascii NOT NULL DEFAULT 'node_generic',
	class_path VARCHAR(60) NOT NULL DEFAULT 'includes/nodes/class.node_generic.php',
	PRIMARY KEY (type_name)
) ENGINE=InnoDB;

CREATE TABLE allowed_type(
	type_name VARCHAR(20) CHARACTER SET ascii NOT NULL,
	allowed_type VARCHAR(20) CHARACTER SET ascii NOT NULL,
	max_num INTEGER DEFAULT -1,
	b_alias BOOLEAN DEFAULT 0,
	FOREIGN KEY(type_name) REFERENCES node_type(type_name),
	FOREIGN KEY(allowed_type) REFERENCES node_type(type_name),
	PRIMARY KEY(type_name,allowed_type)
) ENGINE = InnoDB;

CREATE TABLE node_generic(
	node_id INTEGER NOT NULL AUTO_INCREMENT,
	parent_node_id INTEGER NOT NULL,
	type_name VARCHAR(20) CHARACTER SET ascii NOT NULL,
	title VARCHAR(60),
	creator VARCHAR(20),
	index_num INTEGER DEFAULT 0,
	override_view VARCHAR(64) CHARACTER SET ascii NULL,
	anonymous_read BOOLEAN DEFAULT 0,
	anonymous_add BOOLEAN DEFAULT 0,
	anonymous_edit BOOLEAN DEFAULT 0,
	anonymous_delete BOOLEAN DEFAULT 0,
	inherit_permissions BOOLEAN DEFAULT 1,
	PRIMARY KEY(node_id),
	FOREIGN KEY(parent_node_id) REFERENCES node_generic(node_id),
	FOREIGN KEY(type_name) REFERENCES node_type(type_name)
) ENGINE = InnoDB;
CREATE INDEX titles ON node_generic(title);

CREATE TABLE node_info(
	node_id INTEGER NOT NULL AUTO_INCREMENT,
	content TEXT,
	tags TEXT,
	views INTEGER DEFAULT 0,
	creation DATETIME,
	modification DATETIME,
	PRIMARY KEY(node_id),
	FOREIGN KEY(node_id) REFERENCES node_generic(node_id)
) ENGINE = InnoDB;

CREATE TABLE node_media(
	node_id INTEGER NOT NULL,
	media_type VARCHAR(64),
	filename VARCHAR(255),
	media_url TEXT,
	PRIMARY KEY(node_id),
	FOREIGN KEY(node_id) REFERENCES node_generic(node_id)
) ENGINE = InnoDB;

CREATE TABLE node_user(
	user_id INTEGER NOT NULL AUTO_INCREMENT,
	username VARCHAR(20),
	passwd VARCHAR(40),
	email VARCHAR(30),
	PRIMARY KEY(user_id),
	UNIQUE (username),
	FOREIGN KEY(user_id) REFERENCES node_generic(node_id)
) ENGINE = InnoDB;

CREATE TABLE user_subscription(
	group_id INTEGER NOT NULL,
	user_id INTEGER NOT NULL,
	registered DATETIME,
	FOREIGN KEY(group_id) REFERENCES node_generic(node_id),
	FOREIGN KEY(user_id) REFERENCES node_user(user_id),
	PRIMARY KEY(group_id,user_id)
) ENGINE = InnoDB;

CREATE TABLE sys_session(
	session_id INTEGER NOT NULL AUTO_INCREMENT,
	user_id INTEGER NOT NULL,
	signature TEXT,
	remote_addr TEXT,
	user_agent TEXT,
	expire DATETIME,
	PRIMARY KEY(session_id),
	FOREIGN KEY(user_id) REFERENCES node_user(user_id)
) ENGINE = InnoDB;

CREATE TABLE node_permission(
	entity_id INTEGER NOT NULL,
	node_id INTEGER NOT NULL,
	b_inverse BOOLEAN DEFAULT 0,
	b_read BOOLEAN DEFAULT 0,
	b_add BOOLEAN DEFAULT 0,
	b_edit BOOLEAN DEFAULT 0,
	b_delete BOOLEAN DEFAULT 0,
	b_ownership BOOLEAN DEFAULT 0,
	b_mastership BOOLEAN DEFAULT 0,
	FOREIGN KEY(entity_id) REFERENCES node_generic(node_id),
	FOREIGN KEY(node_id) REFERENCES node_generic(node_id),
	PRIMARY KEY(entity_id,node_id)
) ENGINE = InnoDB;

CREATE TABLE sys_error(
	error_id INTEGER NOT NULL AUTO_INCREMENT,
	error_code INTEGER NOT NULL DEFAULT 0,
	error_date DATETIME,
	error_severity INTEGER DEFAULT 1,
	error_message VARCHAR(255),
	error_file VARCHAR(255),
	error_line INTEGER,
	error_method VARCHAR(100),
	user_id INTEGER,
	user_agent VARCHAR(255),
	remote_addr VARCHAR(16),
	flag TINYINT DEFAULT 0,
	PRIMARY KEY(error_id)
) ENGINE = InnoDB;

CREATE TABLE data_type(
	data_type_name VARCHAR(10) NOT NULL,
	data_regexp TEXT,
	PRIMARY KEY(data_type_name)
) ENGINE = InnoDB;

CREATE TABLE node_data(
	node_id INTEGER NOT NULL,
	data_type_name VARCHAR(10) NOT NULL,
	data_label VARCHAR(20),
	data_value TEXT,
	FOREIGN KEY(node_id) REFERENCES node_generic(node_id),
	FOREIGN KEY(data_type_name) REFERENCES data_type(data_type_name)
) ENGINE = InnoDB;

CREATE TABLE node_alias(
	node_id INTEGER NOT NULL,
	pointed_node_id INTEGER,
	FOREIGN KEY(node_id) REFERENCES node_generic(node_id)
) ENGINE = InnoDB;

INSERT INTO node_type (type_name, class_name, class_path) VALUES ('root', 'node_generic', 'includes/nodes/class.node_generic.php');
INSERT INTO node_type (type_name, class_name, class_path) VALUES ('users', 'node_generic', 'includes/nodes/class.node_generic.php');
INSERT INTO node_type (type_name, class_name, class_path) VALUES ('groups', 'node_generic', 'includes/nodes/class.node_generic.php');
INSERT INTO node_type (type_name, class_name, class_path) VALUES ('user', 'node_user', 'includes/nodes/class.node_user.php');
INSERT INTO node_type (type_name, class_name, class_path) VALUES ('group', 'node_group', 'includes/nodes/class.node_group.php');
INSERT INTO node_type (type_name, class_name, class_path) VALUES ('folder', 'node_generic', 'includes/nodes/class.node_generic.php');
INSERT INTO node_type (type_name, class_name, class_path) VALUES ('menu', 'node_generic', 'includes/nodes/class.node_generic.php');
INSERT INTO node_type (type_name, class_name, class_path) VALUES ('resources', 'node_generic', 'includes/nodes/class.node_generic.php');
INSERT INTO node_type (type_name, class_name, class_path) VALUES ('page', 'node_generic', 'includes/nodes/class.node_generic.php');
INSERT INTO node_type (type_name, class_name, class_path) VALUES ('comment', 'node_generic', 'includes/nodes/class.node_generic.php');
INSERT INTO node_type (type_name, class_name, class_path) VALUES ('blog', 'node_generic', 'includes/nodes/class.node_generic.php');
INSERT INTO node_type (type_name, class_name, class_path) VALUES ('album', 'node_generic', 'includes/nodes/class.node_generic.php');
INSERT INTO node_type (type_name, class_name, class_path) VALUES ('forum', 'node_generic', 'includes/nodes/class.node_generic.php');
INSERT INTO node_type (type_name, class_name, class_path) VALUES ('topic', 'node_generic', 'includes/nodes/class.node_generic.php');
INSERT INTO node_type (type_name, class_name, class_path) VALUES ('post', 'node_generic', 'includes/nodes/class.node_generic.php');
INSERT INTO node_type (type_name, class_name, class_path) VALUES ('wiki', 'node_generic', 'includes/nodes/class.node_generic.php');
INSERT INTO node_type (type_name, class_name, class_path) VALUES ('article', 'node_generic', 'includes/nodes/class.node_generic.php');
INSERT INTO node_type (type_name, class_name, class_path) VALUES ('revision', 'node_generic', 'includes/nodes/class.node_generic.php');
INSERT INTO node_type (type_name, class_name, class_path) VALUES ('video', 'node_media', 'includes/nodes/class.node_media.php');
INSERT INTO node_type (type_name, class_name, class_path) VALUES ('image', 'node_media', 'includes/nodes/class.node_media.php');
INSERT INTO node_type (type_name, class_name, class_path) VALUES ('document', 'node_media', 'includes/nodes/class.node_media.php');
INSERT INTO node_type (type_name, class_name, class_path) VALUES ('icon', 'node_media', 'includes/nodes/class.node_media.php');
INSERT INTO node_type (type_name, class_name, class_path) VALUES ('link', 'node_link', 'includes/nodes/class.node_link.php');
INSERT INTO node_type (type_name, class_name, class_path) VALUES ('alias', 'node_alias', 'includes/nodes/class.node_alias.php');

INSERT INTO allowed_type (type_name,allowed_type) VALUES ('root','folder');
INSERT INTO allowed_type (type_name,allowed_type) VALUES ('root','menu');
INSERT INTO allowed_type (type_name,allowed_type) VALUES ('root','resources');
INSERT INTO allowed_type (type_name,allowed_type) VALUES ('root','page');
INSERT INTO allowed_type (type_name,allowed_type) VALUES ('root','blog');
INSERT INTO allowed_type (type_name,allowed_type) VALUES ('root','album');
INSERT INTO allowed_type (type_name,allowed_type) VALUES ('root','forum');
INSERT INTO allowed_type (type_name,allowed_type) VALUES ('root','wiki');
INSERT INTO allowed_type (type_name,allowed_type) VALUES ('root','icon');
INSERT INTO allowed_type (type_name,allowed_type) VALUES ('menu','link');
INSERT INTO allowed_type (type_name,allowed_type) VALUES ('menu','alias');
INSERT INTO allowed_type (type_name,allowed_type) VALUES ('resources','icon');
INSERT INTO allowed_type (type_name,allowed_type) VALUES ('resources','page');
INSERT INTO allowed_type (type_name,allowed_type) VALUES ('resources','video');
INSERT INTO allowed_type (type_name,allowed_type) VALUES ('resources','image');
INSERT INTO allowed_type (type_name,allowed_type) VALUES ('resources','document');
INSERT INTO allowed_type (type_name,allowed_type) VALUES ('resources','link');
INSERT INTO allowed_type (type_name,allowed_type) VALUES ('resources','alias');
INSERT INTO allowed_type (type_name,allowed_type) VALUES ('users','user');
INSERT INTO allowed_type (type_name,allowed_type) VALUES ('groups','group');
INSERT INTO allowed_type (type_name,allowed_type) VALUES ('user','folder');
INSERT INTO allowed_type (type_name,allowed_type) VALUES ('user','icon');
INSERT INTO allowed_type (type_name,allowed_type) VALUES ('user','image');
INSERT INTO allowed_type (type_name,allowed_type) VALUES ('user','video');
INSERT INTO allowed_type (type_name,allowed_type) VALUES ('user','album');
INSERT INTO allowed_type (type_name,allowed_type) VALUES ('user','document');
INSERT INTO allowed_type (type_name,allowed_type) VALUES ('user','link');
INSERT INTO allowed_type (type_name,allowed_type) VALUES ('user','alias');
INSERT INTO allowed_type (type_name,allowed_type) VALUES ('user','blog');
INSERT INTO allowed_type (type_name,allowed_type) VALUES ('group','icon');
INSERT INTO allowed_type (type_name,allowed_type) VALUES ('group','folder');
INSERT INTO allowed_type (type_name,allowed_type) VALUES ('group','image');
INSERT INTO allowed_type (type_name,allowed_type) VALUES ('group','video');
INSERT INTO allowed_type (type_name,allowed_type) VALUES ('group','album');
INSERT INTO allowed_type (type_name,allowed_type) VALUES ('group','document');
INSERT INTO allowed_type (type_name,allowed_type) VALUES ('group','link');
INSERT INTO allowed_type (type_name,allowed_type) VALUES ('group','alias');
INSERT INTO allowed_type (type_name,allowed_type) VALUES ('group','blog');
INSERT INTO allowed_type (type_name,allowed_type) VALUES ('group','wiki');
INSERT INTO allowed_type (type_name,allowed_type) VALUES ('group','forum');
INSERT INTO allowed_type (type_name,allowed_type) VALUES ('folder','folder');
INSERT INTO allowed_type (type_name,allowed_type) VALUES ('folder','icon');
INSERT INTO allowed_type (type_name,allowed_type) VALUES ('folder','page');
INSERT INTO allowed_type (type_name,allowed_type) VALUES ('folder','blog');
INSERT INTO allowed_type (type_name,allowed_type) VALUES ('folder','album');
INSERT INTO allowed_type (type_name,allowed_type) VALUES ('folder','forum');
INSERT INTO allowed_type (type_name,allowed_type) VALUES ('folder','wiki');
INSERT INTO allowed_type (type_name,allowed_type) VALUES ('folder','video');
INSERT INTO allowed_type (type_name,allowed_type) VALUES ('folder','image');
INSERT INTO allowed_type (type_name,allowed_type) VALUES ('folder','document');
INSERT INTO allowed_type (type_name,allowed_type) VALUES ('folder','link');
INSERT INTO allowed_type (type_name,allowed_type) VALUES ('folder','alias');
INSERT INTO allowed_type (type_name,allowed_type) VALUES ('page','comment');
INSERT INTO allowed_type (type_name,allowed_type) VALUES ('page','icon');
INSERT INTO allowed_type (type_name,allowed_type) VALUES ('page','link');
INSERT INTO allowed_type (type_name,allowed_type) VALUES ('page','alias');
INSERT INTO allowed_type (type_name,allowed_type) VALUES ('page','image');
INSERT INTO allowed_type (type_name,allowed_type) VALUES ('page','video');
INSERT INTO allowed_type (type_name,allowed_type) VALUES ('page','document');
INSERT INTO allowed_type (type_name,allowed_type) VALUES ('blog','page');
INSERT INTO allowed_type (type_name,allowed_type) VALUES ('blog','icon');
INSERT INTO allowed_type (type_name,allowed_type) VALUES ('blog','video');
INSERT INTO allowed_type (type_name,allowed_type) VALUES ('blog','image');
INSERT INTO allowed_type (type_name,allowed_type) VALUES ('blog','document');
INSERT INTO allowed_type (type_name,allowed_type) VALUES ('blog','link');
INSERT INTO allowed_type (type_name,allowed_type) VALUES ('blog','alias');
INSERT INTO allowed_type (type_name,allowed_type) VALUES ('album','icon');
INSERT INTO allowed_type (type_name,allowed_type) VALUES ('album','video');
INSERT INTO allowed_type (type_name,allowed_type) VALUES ('album','image');
INSERT INTO allowed_type (type_name,allowed_type) VALUES ('forum','topic');
INSERT INTO allowed_type (type_name,allowed_type) VALUES ('forum','icon');
INSERT INTO allowed_type (type_name,allowed_type) VALUES ('topic','post');
INSERT INTO allowed_type (type_name,allowed_type) VALUES ('topic','icon');
INSERT INTO allowed_type (type_name,allowed_type) VALUES ('post','video');
INSERT INTO allowed_type (type_name,allowed_type) VALUES ('post','image');
INSERT INTO allowed_type (type_name,allowed_type) VALUES ('post','document');
INSERT INTO allowed_type (type_name,allowed_type) VALUES ('post','link');
INSERT INTO allowed_type (type_name,allowed_type) VALUES ('post','alias');
INSERT INTO allowed_type (type_name,allowed_type) VALUES ('wiki','article');
INSERT INTO allowed_type (type_name,allowed_type) VALUES ('wiki','icon');
INSERT INTO allowed_type (type_name,allowed_type) VALUES ('article','revision');
INSERT INTO allowed_type (type_name,allowed_type) VALUES ('article','icon');
INSERT INTO allowed_type (type_name,allowed_type) VALUES ('article','comment');
INSERT INTO allowed_type (type_name,allowed_type) VALUES ('video','comment');
INSERT INTO allowed_type (type_name,allowed_type) VALUES ('video','icon');
INSERT INTO allowed_type (type_name,allowed_type) VALUES ('image','comment');
INSERT INTO allowed_type (type_name,allowed_type) VALUES ('image','icon');
INSERT INTO allowed_type (type_name,allowed_type) VALUES ('document','comment');
INSERT INTO allowed_type (type_name,allowed_type) VALUES ('document','icon');

INSERT INTO data_type(data_type_name,data_regexp) VALUES ('text','');
INSERT INTO data_type(data_type_name,data_regexp) VALUES ('html','');
INSERT INTO data_type(data_type_name,data_regexp) VALUES ('integer','/^[\+\-]?\d*$/');
INSERT INTO data_type(data_type_name,data_regexp) VALUES ('boolean','/^[01]$/');
INSERT INTO data_type(data_type_name,data_regexp) VALUES ('float','/^[\+\-]?\d*[\.]?\d*$/');
INSERT INTO data_type(data_type_name,data_regexp) VALUES ('datetime','/^[012]\d{3}-?[01]\d-?[0123]\d(T|\s)[012]\d:?[0123456]\d:?[0123456]\d(,\d{1,2})?(Z|[\+\-][012]\d:?[0123456]\d)?$/');
INSERT INTO data_type(data_type_name,data_regexp) VALUES ('timestamp','/^\d*$/');
INSERT INTO data_type(data_type_name,data_regexp) VALUES ('date','/^[012]\d{3}-?[01]\d-?[0123]\d$/');
INSERT INTO data_type(data_type_name,data_regexp) VALUES ('time','/^[012]\d:?[0123456]\d(:?[0123456]\d(,\d{1,2})?)?$/');
INSERT INTO data_type(data_type_name,data_regexp) VALUES ('email','/^\b[\w\.-]+@[\w\.-]+\.\w{2,4}\b$/');

INSERT INTO node_generic(node_id,parent_node_id,type_name,title,creator,index_num,anonymous_read,anonymous_add,inherit_permissions) VALUES (1,1,'root','Home','Admin',1,1,0,0);
INSERT INTO node_generic(node_id,parent_node_id,type_name,title,creator,index_num,anonymous_read,anonymous_add,inherit_permissions) VALUES (2,1,'folder','Sites','Admin',1,1,0,0);
INSERT INTO node_generic(node_id,parent_node_id,type_name,title,creator,index_num,anonymous_read,anonymous_add,inherit_permissions) VALUES (3,1,'users','Users','Admin',2,0,1,0);
INSERT INTO node_generic(node_id,parent_node_id,type_name,title,creator,index_num,anonymous_read,anonymous_add,inherit_permissions) VALUES (4,1,'groups','Groups','Admin',3,0,0,0);
INSERT INTO node_generic(node_id,parent_node_id,type_name,title,creator,index_num,anonymous_read,anonymous_add,inherit_permissions) VALUES (7,3,'user','Admin','Admin',1,0,0,0);

INSERT INTO node_info(node_id,content,creation,modification) VALUES (1,'',NOW(),NOW());
INSERT INTO node_info(node_id,content,creation,modification) VALUES (2,'<p>Start your website here...</p>',NOW(),NOW());
INSERT INTO node_info(node_id,content,creation,modification) VALUES (3,'',NOW(),NOW());
INSERT INTO node_info(node_id,content,creation,modification) VALUES (4,'',NOW(),NOW());
INSERT INTO node_info(node_id,content,creation,modification) VALUES (7,'<p></p>',NOW(),NOW());

INSERT INTO node_user(user_id,username,passwd,email) VALUES(7,'Admin','10a34637ad661d98ba3344717656fcc76209c2f8','admin@localhost');
INSERT INTO node_permission(entity_id,node_id,b_ownership,b_mastership) VALUES (7,1,1,1);
INSERT INTO node_permission(entity_id,node_id,b_ownership,b_mastership) VALUES (7,7,1,1);

INSERT INTO node_data(node_id,data_type_name,data_label,data_value) VALUES (1,'text','lang','fr');
INSERT INTO node_data(node_id,data_type_name,data_label,data_value) VALUES (1,'text','safe_tags','<a><article><audio><b><big><blockquote><br><cite><code><em><embed><h1><h2><h3><h4><h5><h6><hr><i><img><li><ol><p><pre><section><small><span><strong><sub><sup><table><tbody><td><tr><th><ul><video>');
INSERT INTO node_data(node_id,data_type_name,data_label,data_value) VALUES (1,'text','thumbs_directory','secure/thumbs/');
INSERT INTO node_data(node_id,data_type_name,data_label,data_value) VALUES (1,'text','tmp_directory','secure/tmp/');
INSERT INTO node_data(node_id,data_type_name,data_label,data_value) VALUES (1,'text','previews_directory','secure/previews/');
INSERT INTO node_data(node_id,data_type_name,data_label,data_value) VALUES (1,'text','originals_directory','secure/originals/');
INSERT INTO node_data(node_id,data_type_name,data_label,data_value) VALUES (1,'text','icons_directory','images/icons/');
INSERT INTO node_data(node_id,data_type_name,data_label,data_value) VALUES (1,'integer','max_image_width','800');
INSERT INTO node_data(node_id,data_type_name,data_label,data_value) VALUES (1,'integer','max_image_height','600');
INSERT INTO node_data(node_id,data_type_name,data_label,data_value) VALUES (1,'integer','max_thumb_width','160');
INSERT INTO node_data(node_id,data_type_name,data_label,data_value) VALUES (1,'integer','max_thumb_height','160');
INSERT INTO node_data(node_id,data_type_name,data_label,data_value) VALUES (1,'integer','default_icon_size','64');
INSERT INTO node_data(node_id,data_type_name,data_label,data_value) VALUES (1,'boolean','keep_original','1');
INSERT INTO node_data(node_id,data_type_name,data_label,data_value) VALUES (1,'timestamp','session_expire','604800');
INSERT INTO node_data(node_id,data_type_name,data_label,data_value) VALUES (1,'text','timezone','Europe/Paris');


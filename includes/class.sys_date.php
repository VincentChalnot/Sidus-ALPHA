<?php

define('DATE_DEFAULT', 'j/m/y à H:i');
define('DATE_TINY', 'j/m/y');

class sys_date{

	private $config;
	private $type;

	public function __construct(sys_config $config){
		$this->config=$config;
		$this->type=$config->db()->get_type();
	}

	public function now(){
		if($this->type == 'mysql'){
			return '\''.date('c').'\'';
		}
		if($this->type == 'sqlite3' || $this->type == 'sqlite'){
			return '\''.date('c').'\'';
		}
		trigger_error('ABoard : Database type undefined for dates abstraction layer.', E_USER_ERROR);
		exit;
	}

	public function from_timestamp($timestamp){
		$timestamp=(int)$timestamp;
		if($this->type == 'mysql'){
			return 'FROM_UNIXTIME('.$timestamp.')';
		}
		if($this->type == 'sqlite3' || $this->type == 'sqlite'){
			return 'date('.$timestamp.',\'unixepoch\')';
		}
		trigger_error('ABoard : Database type undefined for dates abstraction layer.', E_USER_ERROR);
		exit;
	}

	public function parse($format, $date='Y-m-d'){
		$tmp=date_parse_from_format($format, $date);
		$timestamp=mktime($tmp['hour'], $tmp['minute'], $tmp['second'], $tmp['month'], $tmp['day'], $tmp['year']);
		return $this->from_timestamp($timestamp);
	}

	/**
	 * TODO: Return formated date
	 */
	public function date($datetime, $format=DATE_DEFAULT){
		$timestamp=strtotime($datetime);
		return date($format, $timestamp);
	}

	/**
	 * Return a formated (and localized) string with the elapsed time since the given date
	 */
	public function since($datetime){
		$timestamp=strtotime($datetime);
		$diff=time() - $timestamp;

		if($diff < 60){//If less than one minute
			return $diff.' '.$this->config->localize('seconds');
		}
		if($diff < 3600){//If less than one hour
			$m=floor($diff / 60);
			if($m == 1){
				return '1 '.$this->config->localize('minute');
			}
			return $m.' '.$this->config->localize('minutes');
		}
		if($diff < 86400){//If less than one day
			$h=floor($diff / 3600);
			if($h == 1){
				return '1 '.$this->config->localize('hour');
			}
			return $h.' '.$this->config->localize('hours');
		}

		//If more than one day:

		$day=(int)date('j', $diff);
		$mon=(int)date('n', $diff) - 1;
		$yea=(int)date('Y', $diff) - 1970;

		$tmp=array();

		if($day > 0){
			if($day == 1){
				$tmp[]='1 '.$this->config->localize('day');
			}else{
				$tmp[]=$day.' '.$this->config->localize('days');
			}
		}
		if($mon > 0){
			if($mon == 1){
				$tmp[]='1 '.$this->config->localize('month');
			}else{
				$tmp[]=$mon.' '.$this->config->localize('months');
			}
		}
		if($yea > 0){
			if($yea == 1){
				$tmp[]='1 '.$this->config->localize('year');
			}else{
				$tmp[]=$yea.' '.$this->config->localize('years');
			}
		}

		$separator=array(', ', ' '.$this->config->localize('and').' ', '');
		$final=array();
		foreach($tmp as $value){
			$final[]=$value.array_pop($separator);
		}
		return implode(array_reverse($final));
	}

}

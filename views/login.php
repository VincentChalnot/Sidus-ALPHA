<form class="login_menu" action="<?php echo $board->node()->link('login') ?>" method="post">
	<label for="username"><?php echo $board->localize('Username')?> :</label><input type="text" id="username" name="username" value="<?php echo isset($_POST['username']) ? $board->secure_display($_POST['username']):'' ?>" placeholder="<?php echo $board->localize('Username')?>" />
	<label for="password"><?php echo $board->localize('Password')?> :</label><input type="password" id="password" name="password" placeholder="<?php echo $board->localize('Password')?>" />
	<input class="button" type="reset" name="reset" value="<?php echo $board->localize('Cancel') ?>" title="<?php echo $board->localize('Cancel') ?>" onclick="history.back()"/>
	<input class="button" type="submit" name="login" value="<?php echo $board->localize('Login') ?>" title="<?php echo $board->localize('Login') ?>" />
	<hr/>
	<a class="button" href="<?php echo $board->node()->link('forgot') ?>"><?php echo $board->localize('Forgot your password ?') ?></a>
	<a class="button" href="<?php echo $board->node()->link('subscribe') ?>"><?php echo $board->localize('Subscribe') ?></a>
</form>

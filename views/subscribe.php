<?php $form = $board->get_form() ?>
<?php echo $form->form_tag(array('class'=>'login_menu')) ?>
	<?php foreach($form->get_all() as $input): ?>
		<?php echo $input ?>
	<?php endforeach ?>
	<input class="button" type="reset" name="reset" value="<?php echo $board->localize('Cancel') ?>" title="<?php echo $board->localize('Cancel') ?>" onclick="history.back()"/>
	<input class="button" type="submit" name="submit" value="<?php echo $board->localize('Subscribe') ?>" title="<?php echo $board->localize('Subscribe') ?>" />
</form>
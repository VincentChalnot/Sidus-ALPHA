<?php if(!isset($nodes, $board)){trigger_error('You must set $nodes and $board before loading the following component :'.$_SERVER['SCRIPT_NAME']);return;} ?>
<?php if(!$nodes){return false;} ?>
<div class="icons_view">
	<script type="text/javascript">
		function open_infos(el){
			document.getElementById(el.id+"_infos").style.display="block";
		}
		function close_element(id){document.getElementById(id).style.display="none";}
		function close_infos(el){
			setTimeout("close_element('"+el.id+"_infos')",100);
		}
	</script>
	<?php foreach($nodes as $child): ?>
		<?php if($child->get_auth('read')): ?>
			<div class="node">
				<a href="<?php echo $child->link() ?>" id="inode_<?php echo $child->get('node_id') ?>" title="<?php echo $child ?>" selected="false" onclick="this.focus();return false;" ondblclick="document.location.href=this.href" onfocus="open_infos(this)" onblur="close_infos(this)">
					<?php echo $child->get_html_thumb(ICON_BIG) ?> 
					<?php echo $board->shorten($child, 50) ?>
				</a>
				<div class="infos" id="inode_<?php echo $child->get('node_id') ?>_infos">
					<div class="triangle"></div>
					<a href="<?php echo $child->link() ?>"><?php echo $board->config->localize('Open') ?></a>
					
					<?php if($child->get_auth('edit')): ?>
						<a href="<?php echo $child->link('edit') ?>"><?php echo $board->config->localize('Edit') ?></a>
					<?php endif ?>
						
					<?php if($child->get_auth('delete')): ?>
						<a href="<?php echo $child->link('delete') ?>"><?php echo $board->config->localize('Delete') ?></a>
					<?php endif ?>
					<p>
						<b><?php echo $board->config->localize('Type') ?></b> : <?php echo $board->config->localize($child->get('type_name')) ?><br/>
						<b><?php echo $board->config->localize('Creation') ?></b> : <?php echo $child->get_creation() ?><br/>
						<b><?php echo $board->config->localize('Modification') ?></b> : <?php echo $child->get_modification() ?><br/>
						<?php foreach($child->get_owners() as $owner): ?>
							<b><?php echo $board->config->localize('Owner') ?></b> : <?php echo $owner ?><br/>
						<?php endforeach ?>
						<b><?php echo $board->config->localize('Anonymous rights') ?></b> : 
							<?php echo $child->get('anonymous_read') ? $board->config->localize('Read'):'' ?>
							<?php echo $child->get('anonymous_add') ? $board->config->localize('Add'):'' ?>
							<?php echo $child->get('anonymous_edit') ? $board->config->localize('Edit'):'' ?>
							<?php echo $child->get('anonymous_delete') ? $board->config->localize('Delete'):'' ?><br/>
						<?php if($board->config->user()->is_connected()): ?>
							<b><?php echo $board->config->localize('Your rights') ?></b> :
								<?php echo $child->get_auth('read') ? $board->config->localize('Read'):'' ?>
								<?php echo $child->get_auth('add') ? $board->config->localize('Add'):'' ?>
								<?php echo $child->get_auth('edit') ? $board->config->localize('Edit'):'' ?>
								<?php echo $child->get_auth('delete') ? $board->config->localize('Delete'):'' ?><br/>
						<?php endif ?>
					</p>
				</div>
			</div>
		<?php endif ?>
	<?php endforeach ?>
</div>
<div class="configuration">
	<h2><?php echo $board->localize('Manage account') ?></h2>
	<?php echo $board->node()->action_button('logout') ?>
	<?php echo $board->node()->action_button('admin') ?>
	
	<h2><?php echo $board->localize('Password') ?></h2>
	<form action="<?php echo $board->node()->link() ?>" method="post" onsubmit="return hash_passwd()">
		<input type="hidden" name="changepass" value="true" />
		<label for="passwd_clear"><?php echo $board->localize('Old Password')?> :</label><input class="login_input" type="password" id="passwd_clear" placeholder="Password" />
		<input type="hidden" id="passwd_hashed" name="sha1" />
		<label for="temp1"><?php echo $board->localize('New Password')?> :</label><input class="login_input" type="password" id="temp1" placeholder="Password" />
		<label for="temp2"><?php echo $board->localize('Retype')?> :</label><input class="login_input" type="password" id="temp2" placeholder="Retype password" />
		<input class="button" type="submit" name="submit" value="<?php echo $board->localize('Submit') ?>" title="<?php echo $board->localize('Submit') ?>" />
	</form>
	<script type="text/javascript" src="<?php echo HTTP_PATH ?>scripts/login.js"></script>

	<h2><?php echo $board->localize('Sessions') ?></h2>
	<table class="sessions">
		<tbody>
			<tr>
				<th><?php echo $board->localize('IP Address') ?></th>
				<th><?php echo $board->localize('User Agent') ?></th>
				<th><?php echo $board->localize('Expire') ?></th>
				<th><?php echo $board->localize('Actions') ?></th>
			</tr>
			<?php foreach($board->user()->get_opened_sessions() as $session):?>
				<tr <?php echo ($session['session_id']==$board->config()->session()->get('session_id'))?'class="active"':'' ?>>
					<td><?php echo $session['remote_addr'] ?></td>
					<td><?php echo $board->secure_display($session['user_agent']) ?></td>
					<td><?php echo $session['expire'] ?></td>
					<td>
						<?php if($session['session_id']!=$board->config()->session()->get('session_id')): ?>
							<a href="<?php echo $board->node()->link(array('config', 'delete_session'=>$session['session_id'])) ?>" <?php echo $board->generate_icon('delete') ?></a>
						<?php else: ?>
							<p class="empty">Active session</p>
						<?php endif ?>
					</td>
				</tr>
			<?php endforeach ?>
		</tbody>
	</table>
	
	
	<h2><?php echo $board->localize('Security') ?></h2>
	
	<h2><?php echo $board->localize('Other') ?></h2>
	<p>User Data:</p>
	<pre><?php echo var_dump($board->user()->get_all_data()) ?></pre>
</div>